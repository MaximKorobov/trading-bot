#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import numpy as np
import os
import statsmodels.api as sm

from main.config.config import Config
from main.source.quandl.quandl import Quandl
from main.source.yahoo.share import Share
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType

if __name__ == '__main__':

    # ticker = 'BRENT'
    load_oil = True
    ticker = 'WTI'

    t_share = "BMW.DE"
    # from_date, to_date = Date.years_dates(2005, 2016)
    from_date, to_date = Date.last_years(2)

    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)

    # Oil data
    quandl_section = config.config_section_map('SourceQuandl')
    quandl = Quandl(quandl_section['api_key'], "CHRIS/CME_CL1")
    oil_price = quandl.get_history(from_date, to_date)

    # dta = sm.datasets.co2.load_pandas().data
    # dta.co2.interpolate(inplace=True)

    # # res = sm.tsa.seasonal_decompose(dta.co2)
    # # res = sm.tsa.seasonal_decompose(df_share["BMW.DE"], freq='D')
    # res = sm.tsa.seasonal_decompose(df_share.T)
    # resplot = res.plot()

    # dta = pandas.Series([x%3 for x in range(100)])
    # decomposed = sm.tsa.seasonal_decompose(np.asarray(dta), freq=3)
    if load_oil:
        dta = oil_price["Last"]
        decomposed = sm.tsa.seasonal_decompose(np.asarray(dta), freq=30)
    else:
        keep_index = False
        df = Share(t_share).load_data(from_date, to_date, keep_index=keep_index)
        if keep_index:
            dta = df
        else:
            dta = df
        decomposed = sm.tsa.seasonal_decompose(np.asarray(dta), freq=365)

    figure = decomposed.plot()
    figure.set_size_inches(12, 8)
    # resplot.show()
    folder_name = ResourceLocator.get_resource(ResourceType.OutFolder)
    file_name = folder_name + "season.png"
    if not os.path.isdir(folder_name):
        os.makedirs(folder_name)
    if os.path.isfile(file_name):
        os.remove(file_name)
    figure.savefig(file_name)
