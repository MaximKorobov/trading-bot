#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import matplotlib.pyplot as plt
import numpy as np

from main.config.config import Config
from main.lab.profile.company import Volkswagen
from main.lab.profile.index import Dax
from main.report.report import Report, ReportMode
from main.source.quandl.quandl import Quandl
from main.tool.data.daily_return import DailyReturn
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType


class SharesInfo(Report):

    def get_name(self):
        return "Shares info"

    def __prepare_data__(self):
        def prepare_df(df, ticker):
            df[ticker] = df["Open"]

            for col in ['Open', 'Low', "High", "Close", "Volume", "Settle", "Prev. Day Open Interest"]:
                if col in df.columns:
                    df = df.drop(col, axis=1)

            # df = df.fillna(0)

            return df

        # Load data
        quandl_section = self.config.config_section_map('SourceQuandl')
        quandl_api_key = quandl_section['api_key']
        quandl = Quandl(quandl_api_key, t_share)

        df_share = quandl.get_history(self.params.from_date, self.params.to_date)
        df_share = prepare_df(df_share, self.params.t_share.ticker)
        dr_share = DailyReturn(df_share).compute_daily_returns()

        # Simple plot
        ax = df_share.plot(title=self.params.t_share.ticker, grid=True)
        ax.set_xlabel("Date")
        ax.set_ylabel("Price")
        plt.show()

        # Histogram
        ax = dr_share.plot(title=self.params.t_share.ticker, grid=True)
        ax.set_xlabel("Date")
        ax.set_ylabel("Price")
        dr_share.hist(bins=50)
        mean = dr_share[self.params.t_share.ticker].mean()
        st_d = dr_share[self.params.t_share.ticker].std()
        plt.axvline(mean, color='w')
        plt.axvline(-st_d, color='r')
        plt.axvline(st_d, color='r')
        plt.show()

        # Scatter plot
        quandl = Quandl(quandl_api_key, self.params.t_index)
        df_index = quandl.get_history(self.params.from_date, self.params.to_date)
        df_index = prepare_df(df_index, self.params.t_index.ticker)
        dr_index = DailyReturn(df_index).compute_daily_returns()

        df_all = df_index.join(df_share)
        dr_all = DailyReturn(df_all).compute_daily_returns()
        # dr_all.plot(title="Scatter plot", kind='scatter', x=self.params.t_index.ticker, y=self.params.t_share.ticker, grid=True)

        # Alpha, beta
        # beta, alpha = np.polyfit(dr_all[self.params.t_index.ticker], dr_all[self.params.t_share.ticker], 1)
        beta, alpha = np.polyfit(df_all[self.params.t_index.ticker], df_all[self.params.t_share.ticker], 1)
        plt.plot(dr_all[self.params.t_index.ticker], beta * dr_all[self.params.t_index.ticker] + alpha, '-', color='r')

        print("beta %s | alpha %s" % (beta, alpha))
        print(dr_all.corr(method='pearson'))

        plt.show()


t_share, t_index = (Volkswagen(), Dax())
from_date, to_date = Date.year_dates(2016)


report = SharesInfo(ReportMode.Report)
report.set_params(t_share=t_share, t_index=t_index, from_date=from_date, to_date=to_date)
report.report()

