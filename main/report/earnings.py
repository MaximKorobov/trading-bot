#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime

from main.lab.profile.company.all import get_all_companies
from main.report.report import Report, ReportMode
from main.source.investing.earning import Loader
from main.tool.date import Date


class Earnings(Report):
    def get_name(self):
        return "Earnings"

    def __prepare_data__(self):
        now = datetime.datetime.now()

        loader = Loader(return_prediction=True)
        for data_source in self.params.data_sources:
            earnings_source = data_source.earnings_source
            if earnings_source:
                last_earnings = loader.get_data(earnings_source, 0, now + datetime.timedelta(days=365))
                latest_earning = last_earnings[0]

                next_days = now + datetime.timedelta(days=forward_days)
                if now < latest_earning.release_date < next_days:
                    self.text += "\n %s at %s, %s" % (latest_earning.name, Date.date_str(latest_earning.release_date),
                                                      latest_earning.release_date.strftime('%A'))
                else:
                    print("No earning for %s (next %s days). Next date: %s" %
                          (data_source.name, forward_days, Date.date_str(latest_earning.release_date)))
            else:
                print("Empty earning source for company %s" % data_source.name)


# Settings
forward_days = 7
data_sources = get_all_companies()

report = Earnings(ReportMode.Report)
report.set_params(data_sources=data_sources, forward_days=forward_days)
report.report()
