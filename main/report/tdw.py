#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import calendar
import datetime
import os

import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, Formatter

from main.lab.profile.company import Apple, Netflix, BMW, Daimler, Volkswagen
from main.lab.profile.index import Dax, Index
from main.report.report import Report, ReportMode
from main.source.quandl.quandl import Quandl
from main.tool.data.daily_return import DailyReturn
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType


class DayFormatter(Formatter):
    def __call__(self, x, pos=None):
        index = int(x)
        return calendar.day_name[index]


class TDW(Report):

    @staticmethod
    def get_data_field(source):
        if isinstance(source, Index):
            return "Open"
        else:
            return "Close"

    def get_name(self):
        return "TDW on " + datetime.datetime.now().strftime("%Y-%m-%d")

    def __load_data__(self, source, from_date, to_date, func=None):
        quandl_section = self.config.config_section_map('SourceQuandl')
        quandl_api_key = quandl_section['api_key']
        quandl = Quandl(quandl_api_key, source)

        # Load data
        df_share = quandl.get_history(from_date, to_date)

        # Prepare data
        dr_share = DailyReturn(df_share).compute_daily_returns()

        def date_to_day(date):
            # return datetime.datetime.strptime(date, "%Y-%m-%d").date().weekday()
            return date.date().weekday()

        dr_share['Day'] = dr_share.index.map(date_to_day)

        group_by_day = dr_share.groupby(['Day'])
        field = self.get_data_field(source)
        if func == 'sum':
            data = group_by_day[field].sum()
        else:
            data = group_by_day[field].mean()

        data *= 100

        self.data = data

    def __prepare_data__(self):

        fig = plt.figure(figsize=(8, 8))
        for date_range_index, date_range in enumerate(date_ranges):
            from_date, to_date = date_range

            all_data = []
            for company in self.params.companies:
                self.__load_data__(company, from_date, to_date, 'sum')
                all_data.append(self.data)

            # Plot data
            ax = fig.add_subplot(len(date_ranges), 1, date_range_index + 1)
            ax.xaxis.grid(color='k', linewidth=.5, which='major')
            ax.yaxis.grid(color='k', linewidth=.5, which='major')
            # plt.axhline(0, color='r')
            from_date_str, to_date_str = Date.dates_str(from_date, to_date)
            ax.set_title(from_date_str + ' - ' + to_date_str)

            for index, company in enumerate(companies):
                data = all_data[index]
                ax.plot(data, label=company.name)
                ax.xaxis.set_major_formatter(DayFormatter())
                ax.xaxis.set_major_locator(LinearLocator(numticks=len(data)))

            if date_range_index == 0:
                ax.legend()

        plt.tight_layout()

        folder_name = ResourceLocator.get_resource(ResourceType.OutFolder)
        file_name = folder_name + "tdw.png"
        if not os.path.isdir(folder_name):
            os.makedirs(folder_name)
        if os.path.isfile(file_name):
            os.remove(file_name)
        plt.savefig(file_name)

        self.images = [file_name]

# Settings
# companies = [Apple(), Netflix()] # TODO: broken company info
companies = [Dax(), Daimler(), BMW(), Volkswagen()]
date_ranges = [Date.year_dates(2017), Date.year_dates(2016), Date.year_dates(2015),
               Date.year_dates(2014), Date.year_dates(2013)]


report = TDW(ReportMode.Report)
report.set_params(companies=companies,
                  date_ranges=date_ranges)
report.report()
