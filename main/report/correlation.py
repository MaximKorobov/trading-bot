import investpy
import pandas as pd
import matplotlib.pyplot as plt

from main.tool.date import Date

lys, lye = Date.last_years(1, '%d/%m/%Y')

df_oil = investpy.get_commodity_historical_data('Brent Oil', lys, lye, 'united kingdom')
df_rosn = investpy.get_stock_historical_data('ROSN', 'russia', lys, lye)
df_sber = investpy.get_stock_historical_data('SBER', 'russia', lys, lye)
df_exxon = investpy.get_stock_historical_data('XOM', 'united states', lys, lye)

ohlc = ["Open", "High", "Low", "Close"]

df_oil.columns = [str(col) + '_oil' for col in df_oil.columns]
df_rosn.columns = [str(col) + '_rosneft' for col in df_rosn.columns]
df_sber.columns = [str(col) + '_sber' for col in df_sber.columns]
df_exxon.columns = [str(col) + '_exxon' for col in df_exxon.columns]

df = pd.concat([df_oil, df_rosn, df_sber, df_exxon], axis=1)
print(df)

fig = plt.figure()

ax1 = fig.add_subplot(311)
df['Close_oil'].plot.line()

lines, labels = ax1.get_legend_handles_labels()
ax1.legend(lines, ['Brent oil'], loc='upper left')
ax1.grid(True)
ax1.set_title('Oil')
plt.xticks(rotation=0, ha="center")

ax2 = fig.add_subplot(312)
df['Close_rosneft'].plot.line()
df['Close_sber'].plot.line()
df['Close_exxon'].plot.line()

lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines2, ['Rosneft', 'Sberbank', 'Exxon Mobile'], loc='upper left')
ax2.grid(True)
ax2.set_title('Stocks')
plt.xticks(rotation=0, ha="center")

ax3 = fig.add_subplot(313)
df['Relative'] = df['Close_rosneft'] / df['Close_oil']
df['Relative'].plot.line()
df['Relative'] = df['Close_sber'] / df['Close_oil']
df['Relative'].plot.line()
df['Relative'] = df['Close_exxon'] / df['Close_oil']
df['Relative'].plot.line()

lines3, labels3 = ax3.get_legend_handles_labels()
ax3.legend(lines3, ['Rosneft', 'Sberbank', 'Exxon Mobile'], loc='upper left')
ax3.grid(True)
ax3.set_title('Stock price / Oil price')
plt.xticks(rotation=0, ha="center")

plt.tight_layout()
mng = plt.get_current_fig_manager()
mng.window.showMaximized()
plt.show()
