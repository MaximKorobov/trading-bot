#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
from sklearn import cluster, covariance, manifold

from main.lab.profile.company import Netflix, Apple, Intel, Google, Daimler, Volkswagen
from main.report.report import Report, ReportMode
from main.source.quandl.quandl import Quandl
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType


class MarketStructure(Report):

    def get_name(self):
        return "Market structure on " + datetime.datetime.now().strftime("%Y-%m-%d")

    @staticmethod
    def intersect(list_1, list_2):
        return list(set(list_1) & set(list_2))

    def get_common_dates(self, quotes):
        dates_all = quotes[0][0]
        for quote in quotes:
            dates_symbol = quote[0]
            dates_all = MarketStructure.intersect(dates_all, dates_symbol)
        dates_all = [date.date() for date in dates_all]

        return dates_all

    def make_image(self, plot, quotes, plot_name, names):

        dates_all = self.get_common_dates(quotes)
        #
        # for index, quote in enumerate(quotes):
        #     # filtered = [i for i in quote if i.index in dates_all]
        #     filtered = []
        #     for line in quote.iterrows():
        #         dt = line[0].to_pydatetime()
        #         if dt in dates_all:
        #             filtered.append(line)
        #     quotes[index] = np.stack(filtered)

        open = np.array([q['Open'] for q in quotes]).astype(np.float)
        close = np.array([q['Close'] for q in quotes]).astype(np.float)

        # The daily variations of the quotes are what carry most information
        variation = close - open

        ###############################################################################
        # Learn a graphical structure from the correlations
        edge_model = covariance.GraphLassoCV()

        # standardize the time series: using correlations rather than covariance
        # is more efficient for structure recovery
        X = variation.copy().T
        X /= X.std(axis=0)
        edge_model.fit(X)

        ###############################################################################
        # Cluster using affinity propagation

        _, labels = cluster.affinity_propagation(edge_model.covariance_)
        n_labels = labels.max()

        for i in range(n_labels + 1):
            print('Cluster %i: %s' % ((i + 1), ', '.join(names[labels == i])))

        ###############################################################################
        # Find a low-dimension embedding for visualization: find the best position of
        # the nodes (the stocks) on a 2D plane

        # We use a dense eigen_solver to achieve reproducibility (arpack is
        # initiated with random vectors that we don't control). In addition, we
        # use a large number of neighbors to capture the large-scale structure.
        node_position_model = manifold.LocallyLinearEmbedding(
            n_components=2, eigen_solver='dense', n_neighbors=6)

        embedding = node_position_model.fit_transform(X.T).T

        ###############################################################################
        # Visualization
        plt.figure(1, facecolor='w', figsize=(10, 8))
        plt.clf()
        ax = plt.axes([0., 0., 1., 1.])
        plt.axis('off')

        # Display a graph of the partial correlations
        partial_correlations = edge_model.precision_.copy()
        d = 1 / np.sqrt(np.diag(partial_correlations))
        partial_correlations *= d
        partial_correlations *= d[:, np.newaxis]
        non_zero = (np.abs(np.triu(partial_correlations, k=1)) > 0.02)

        # Plot the nodes using the coordinates of our embedding
        plt.scatter(embedding[0], embedding[1], s=100 * d ** 2, c=labels,
                    cmap=plt.cm.spectral)

        # Plot the edges
        start_idx, end_idx = np.where(non_zero)
        # a sequence of (*line0*, *line1*, *line2*), where::
        #            linen = (x0, y0), (x1, y1), ... (xm, ym)
        segments = [[embedding[:, start], embedding[:, stop]]
                    for start, stop in zip(start_idx, end_idx)]
        values = np.abs(partial_correlations[non_zero])
        lc = LineCollection(segments,
                            zorder=0, cmap=plt.cm.hot_r,
                            norm=plt.Normalize(0, .7 * values.max()))
        lc.set_array(values)
        lc.set_linewidths(15 * values)
        ax.add_collection(lc)

        # Add a label to each node. The challenge here is that we want to
        # position the labels to avoid overlap with other labels
        for index, (name, label, (x, y)) in enumerate(
                zip(names, labels, embedding.T)):

            dx = x - embedding[0]
            dx[index] = 1
            dy = y - embedding[1]
            dy[index] = 1
            this_dx = dx[np.argmin(np.abs(dy))]
            this_dy = dy[np.argmin(np.abs(dx))]
            if this_dx > 0:
                horizontalalignment = 'left'
                x = x + .002
            else:
                horizontalalignment = 'right'
                x = x - .002
            if this_dy > 0:
                verticalalignment = 'bottom'
                y = y + .002
            else:
                verticalalignment = 'top'
                y = y - .002
            plt.text(x, y, name, size=10,
                     horizontalalignment=horizontalalignment,
                     verticalalignment=verticalalignment,
                     bbox=dict(facecolor='w',
                               edgecolor=plt.cm.spectral(label / float(n_labels)),
                               alpha=.6))

        plt.xlim(embedding[0].min() - .15 * embedding[0].ptp(),
                 embedding[0].max() + .10 * embedding[0].ptp(), )
        plt.ylim(embedding[1].min() - .03 * embedding[1].ptp(),
                 embedding[1].max() + .03 * embedding[1].ptp())

        file_name = ResourceLocator.get_resource(ResourceType.OutFolder) + plot_name + '.png'
        plt.savefig(file_name)

        if plot:
            plt.show()

        return file_name

    def __prepare_data__(self):
        quandl_section = self.config.config_section_map('SourceQuandl')
        quandl_api_key = quandl_section['api_key']

        plot = False

        file_names = []
        names = [company.ticker for company in self.params.companies]

        symbols, names = np.array(list(symbol_dict.items())).T

        for data in all_dates:
            from_date, to_date = data[0]
            title = data[1]
            print()
            print('Period: %s - %s' % (Date.date_str(from_date), Date.date_str(to_date)))

            all_quotes = []
            for company in self.params.companies:
                quandl = Quandl(quandl_api_key, company)
                data = quandl.get_history(from_date, to_date)
                # data['Date'] = data.index.to_datetime()
                data.reset_index(level=0, inplace=True)
                data = [tuple(x) for x in data.values]
                all_quotes.append(data, )

            file_name = self.make_image(plot, all_quotes, title, names)
            file_names.append(file_name)


# symbol_dict = {
#     '^GDAXI': 'DAX',
#     'CBK.DE': 'Commerzbank',
#     'BMW.DE': 'BMW',
#     'VOW.DE': 'Volkswagen',
#     'DAI.DE': 'Daimler',
#     'EOAN.DE': 'E. ON',
#     'ALV.DE': 'Allianz',
#     'DBK.DE': 'Deutsche Bank',
#     'RWE.DE': 'RWE'
# }

symbol_dict = {
    'TOT': 'Total',
    'XOM': 'Exxon',
    'CVX': 'Chevron',
    'COP': 'ConocoPhillips'}

companies = [Netflix(), Apple(), Intel(), Google(), Daimler(), Volkswagen()]

all_dates = [
    (Date.last_years(1), 'Market structure - last year'),
    (Date.last_months(1), 'Market structure - Last month'),
    (Date.last_weeks(1), 'Market structure - Last week')
]

report = MarketStructure(ReportMode.Report)
report.set_params(companies=companies, dates=all_dates)
report.report()

