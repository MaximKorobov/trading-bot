#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from datetime import datetime

from tabulate import tabulate

from main.report.report import Report, ReportMode
from main.source.investing.index import Loader
from main.source.investing.index.source import Dax, SP500, Nasdaq


class BigMovement(Report):

    def get_name(self):
        return "Big movement on " + datetime.now().strftime("%Y-%m-%d")

    def __prepare_data__(self):
        data_sources = self.params.data_sources
        offline_mode = self.params.offline_mode

        items_to_alert = []
        for data_source in data_sources:
            index_loader = Loader()
            data = index_loader.get_data(data_source, offline_mode, datetime.now())

            items = []
            for item in data:
                if abs(float(item.change_percent)) > 2:
                    items.append(item)
            items_to_alert.append(items)

        if len(items_to_alert):
            email_text = ""
            for index, group in enumerate(items_to_alert):
                group_name = data_sources[index].name
                email_text += "<h1>" + group_name + "</h1>"
                columns = ["Name", "Last", "Change", "Change in %", "Date/Time"]

                print_data = []
                for item in group:
                    print_data.append([item.name, item.last, item.change, item.change_percent, item.time])

                print()
                print(group_name)
                print(tabulate(print_data, columns))

                email_text += tabulate(print_data, columns, tablefmt="html")

            email_text = email_text.replace("\n", "")
            self.text = email_text


# Settings
report = BigMovement(ReportMode.Report)
report.set_params(data_sources=[Dax(), SP500(), Nasdaq()], offline_mode=0)
report.report()
