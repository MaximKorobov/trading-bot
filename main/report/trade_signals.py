#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from main.lab.backtrader.trade_signals import find_signals, get_pass_date
from main.lab.profile.company import Apple, Intel, Netflix, Google
from main.lab.strategy.calendar import TDW
from main.lab.strategy.ml.regression import MLP, Linear
from main.report.report import Report, ReportMode
from main.tool.date import Date
from main.lab.strategy.indicator import RSISimple, RSIBuySell, HolyGrail, SMA_RSI, SMACross, MACD_ADX, BBands, \
    SimilarBars
from backtrader import num2date


class TradeSignals(Report):

    def get_name(self):
        return "Trade signals"

    @staticmethod
    def format_signal(strategy_name, signal, action):
        return "%s at %s: %s" % (strategy_name, Date.date_str(num2date(signal.dt)), action)

    def __prepare_data__(self):

        # Strategies
        for company in self.params.companies:
            print("\nTicker: %s" % company.ticker)
            pass_date = get_pass_date(self.params.days_to_see)
            print("Pass date: %s" % Date.date_str(pass_date))

            buy_signals = []
            sell_signals = []
            strategies = [RSISimple, RSIBuySell, SMA_RSI, SMACross, HolyGrail, MACD_ADX, BBands, SimilarBars,
                          TDW,
                          Linear, MLP]
            for strategy in strategies:
                print("\tStrategy: %s" % strategy.__name__)

                strategy_names, signals = \
                    find_signals(self.config, company, [strategy],
                                 self.params.from_date, self.params.to_date,
                                 pass_date, self.params.test_date)

                for index, strategy_name in enumerate(strategy_names):
                    if len(signals[index]) % 2 == 1:
                        signal = signals[index][-1].executed
                        if signal.size > 0:
                            action = "buy"
                            buy_signals.append((strategy_name, signal))
                        else:
                            action = "sell"
                            sell_signals.append((strategy_name, signal))

                        print("\t\t" + self.format_signal(strategy_name, signal, action))

            # Report
            signals_count_filter = self.params.signals_count_filter

            if len(buy_signals) >= signals_count_filter or len(sell_signals) >= signals_count_filter:
                if self.text:
                    self.text += "\n\n"
                self.text += company.ticker + ":"

            if len(buy_signals) >= signals_count_filter:
                for buy_signal in buy_signals:
                    self.text += "\n" + self.format_signal(buy_signal[0], buy_signal[1], action)

            if len(sell_signals) >= signals_count_filter:
                for sell_signal in sell_signals:
                    self.text += "\n" + self.format_signal(sell_signal[0], sell_signal[1], action)

# Settings
companies = [Netflix(), Apple(), Intel(), Google()]
from_date, to_date = Date.last_years(1)
test_percents = 80
test_date = Date.dates_delta(from_date, to_date, test_percents)
days_to_see = 1
signals_count = 2

report = TradeSignals(ReportMode.Report)
report.set_params(companies=companies,
                  from_date=from_date, to_date=to_date, test_date=test_date,
                  days_to_see=days_to_see, signals_count_filter=signals_count)
report.report()
