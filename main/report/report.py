#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
from abc import abstractmethod

from main.config.config import Config
from main.notification.email import Email
from main.tool.code.map import Map
from main.tool.hardware import Hardware
from main.tool.resource_locator import ResourceLocator, ResourceType


class ReportMode:
    Debug, Report = range(2)


class ReportChannel:
    Email, SMS = range(2)


class Report:
    def __init__(self, mode, channels=(ReportChannel.Email, )):
        config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
        self.config = Config(config_path)

        # Settings
        self.generation_time = None
        self.mode = mode
        self.channels = channels
        self.params = None

        # Data
        self.text = ""
        self.images = None

    def report(self):
        self.__prepare_data__()
        self.__send_report__()

    def set_params(self, **kwargs):
        self.params = Map(kwargs)

    @abstractmethod
    def get_name(self):
        pass

    @abstractmethod
    def __prepare_data__(self):
        pass

    def __send_report__(self):
        self.generation_time = datetime.datetime.now()

        if self.mode == ReportMode.Debug:
            print("\n--------------\n")
            print(self.text)
        else:
            if self.text:
                report_name = self.get_name()
                if ReportChannel.Email in self.channels:
                    email_section = self.config.config_section_map('NotificationEmail')
                    email_notifications = Email(email_section)

                    email_notifications.notify_message(self.text, report_name, self.images)

    def __add_to_all__(self):
        result = Hardware.status()
        result += "\n Time: %s" % (datetime.datetime.now() - self.generation_time).seconds

        return result
