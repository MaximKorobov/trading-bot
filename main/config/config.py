#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from configparser import ConfigParser
import os


class Config:

    def __init__(self, file):
        abs_path = os.path.abspath(file)

        self.config = ConfigParser()
        self.config.read(abs_path)

    def config_section_map(self, section):
        dict1 = {}
        options = self.config.options(section)
        for option in options:
            dict1[option] = self.config.get(section, option, fallback=None)
        return dict1

    def sections(self):
        return self.config.sections()
