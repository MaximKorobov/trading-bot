#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json

from pyoanda import Client, PRACTICE


class Trader:
    def __init__(self, demo):
        self.demo = demo
        if self.demo == 0:
            self.client = Client(
                environment=PRACTICE,
                account_id="6565117", # API v1 key was generated by OANDA support team
                access_token="1b0a8f1cf72155aca5a4566ce3b3a5ad-a71743dfc8fb1fd5ae050f4bf6758de5"
            )

    def test(self):
        if self.demo == 0:
            # Get account and it's properties
            accounts = self.client.get_accounts("MaximKorobov")["accounts"]
            print(json.dumps(accounts, indent=2))

            for account in accounts:
                print(json.dumps(account["accountCurrency"], indent=2))

            # Get instruments available
            instruments = self.client.get_instruments()
            print(json.dumps(instruments, indent=2))

            # Get all positions
            positions = self.client.get_positions()
            print(json.dumps(positions, indent=2))

            brent_ticker = "BCO_USD"
            # Get instrument history
            history = self.client.get_instrument_history(brent_ticker, granularity="D")
            print(json.dumps(history, indent=2))

            # Get instruments prices (ONCE)
            prices = self.client.get_prices(brent_ticker, False)
            print(json.dumps(prices, indent=2))

            # Get instruments prices (STREAM)
            # events = self.client.get_prices(brent_ticker, True)
            # for chunk in events.iter_content(2054):
            #     print(chunk.decode())
        else:
            print("Demo mode in trader")
