#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import os
import pickle
import re

from main.tool.date import Date


class BlobCache:
    @staticmethod
    def __get_file_name__(ticker, from_date, to_date, folder=None):
        from_date_str, to_date_str = Date.dates_str(from_date, to_date)

        file_name = "%s_%s_%s.pkl" % (ticker, from_date_str, to_date_str)

        file_name = re.sub('[^\w_.)( -]', '', file_name)
        if folder:
            file_name = folder + '/' + file_name

        abs_path = os.path.abspath(file_name)
        return abs_path

    @staticmethod
    def load_history(ticker, from_date, to_date, folder=None):
        data = None

        now = datetime.datetime.now()
        if from_date <= now and to_date <= now:
            file_name = BlobCache.__get_file_name__(ticker, from_date, to_date, folder)

            if os.path.isfile(file_name):
                f = open(file_name, "rb")
                data = pickle.load(f)
                f.close()
        else:
            pass
            # print("Bad dates: %s, %s, %s" % (from_date, to_date, now))

        return data

    @staticmethod
    def save_history(data, ticker, from_date, to_date, folder=None):
        result = False

        now = datetime.datetime.now()
        if from_date <= now and to_date <= now:
            file_name = BlobCache.__get_file_name__(ticker, from_date, to_date, folder)

            if not os.path.isfile(file_name):
                f = open(file_name, "wb")
                pickle.dump(data, f, 2)
                f.close()

        return result
