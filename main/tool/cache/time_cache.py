#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import os

import re

import pickle


class TimeCache:
    @staticmethod
    def get_file_name(ticker, folder=None):

        file_name = "%s.pkl" % ticker

        file_name = re.sub('[^\w_.)( -]', '', file_name)
        if folder:
            file_name = folder + '/' + file_name
        return file_name

    @staticmethod
    def load_history(ticker, save_date, folder=None):
        data = None

        now = datetime.datetime.now()
        file_name = TimeCache.get_file_name(ticker, folder)
        if os.path.isfile(file_name):
            file_modify_timestamp = os.path.getmtime(file_name)
            file_modify_date = datetime.datetime.fromtimestamp(file_modify_timestamp)
            if save_date <= file_modify_date:
                data = pickle.load(open(file_name, "rb"))

        return data

    @staticmethod
    def save_history(data, ticker, folder=None):
        result = False

        file_name = TimeCache.get_file_name(ticker, folder)

        if not os.path.isfile(file_name):
            pickle.dump(data, open(file_name, "wb"))

        return result
