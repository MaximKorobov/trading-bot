#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import dateutil.relativedelta as rd
from backtrader import date2num
from backtrader import num2date


class Date:

    @staticmethod
    def years_dates(from_year, to_year):
        from_date = datetime.datetime(from_year, 1, 1)
        to_date = datetime.datetime(to_year, 12, 31)

        return from_date, to_date

    @staticmethod
    def year_dates(year):
        return Date.years_dates(year, year)

    @staticmethod
    def since_year(from_year):
        from_date = datetime.datetime(from_year, 1, 1)
        to_date = datetime.datetime.now()

        return from_date, to_date

    @staticmethod
    def dates_delta(from_date, to_date, percents):
        from_date_num = date2num(from_date)
        delta = (date2num(to_date) - from_date_num) * percents / 100

        result = num2date(from_date_num + delta)
        return result

    @staticmethod
    def dates_avg(from_date, to_date):
        avg_date = Date.dates_delta(from_date, to_date, 50)

        return avg_date

    @staticmethod
    def dates_str(from_date, to_date):

        from_date_str = Date.date_str(from_date)
        to_date_str = Date.date_str(to_date)

        return from_date_str, to_date_str

    @staticmethod
    def date_str(date, formatter=None):
        fmt = "%Y-%m-%d"
        date_str = date.strftime(formatter) if formatter else date.strftime(fmt)
        return date_str

    @staticmethod
    def date_time_str(date):
        fmt = "%Y-%m-%d %H:%M"
        date_str = date.strftime(fmt)
        return date_str

    @staticmethod
    def time_str(date):
        fmt = "%H:%M:%S"
        date_str = date.strftime(fmt)
        return date_str

    @staticmethod
    def last_months(months=1):
        to_date = datetime.datetime.now()
        from_date = to_date + rd.relativedelta(months=-months)

        return from_date, to_date

    @staticmethod
    def last_years(years=1, formatter=None):
        to_date = datetime.datetime.now()
        from_date = to_date + rd.relativedelta(years=-years)
        if formatter:
            return Date.date_str(from_date, formatter), Date.date_str(to_date, formatter)
        else:
            return from_date, to_date

    @staticmethod
    def last_weeks(weeks=1):
        to_date = datetime.datetime.now()
        from_date = to_date + rd.relativedelta(weeks=-weeks)

        return from_date, to_date
