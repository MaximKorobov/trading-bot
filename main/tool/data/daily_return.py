#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class DailyReturn:
    def __init__(self, data):
        self.df = data

    # Daily returns
    def compute_daily_returns(self):
        daily_returns = self.df.copy()
        daily_returns[1:] = ((self.df[1:] / self.df[:-1].values) - 1)
        daily_returns.ix[0, :] = 0

        return daily_returns
