#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

from datetime import datetime

import pandas
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter, WeekdayLocator, DayLocator, MONDAY, date2num
from mplfinance import candlestick_ohlc
from matplotlib.ticker import FuncFormatter

from main.config.config import Config
from main.notification.email import Email
from main.tool.resource_locator import ResourceLocator, ResourceType

csv_date_format = '%Y-%m-%d'


def str_to_float_date(x):
    date = datetime.strptime(x, csv_date_format)
    float_date = date2num(date)
    return float_date


def millions(x, pos):
    return '%1.0fM' % (x * 1e-6)


def dollars(x, pos):
    return '$%1.0f' % x


def plot_and_save(file):
    # https://finance.yahoo.com/quote/NFLX/history?ltr=1
    ticker = 'Netflix'
    data = pandas.read_csv("./../source/yahoo/demo/netflix-2016-11.csv", header=0, skiprows=0)
    from_date = datetime.strptime(data["Date"].iloc[-1], csv_date_format)
    to_date = datetime.strptime(data["Date"].iloc[0], csv_date_format)
    data["Date"] = data["Date"].apply(str_to_float_date)

    mondays = WeekdayLocator(MONDAY)
    all_days = DayLocator()
    date_format = '%d.%m'
    week_formatter = DateFormatter(date_format)
    day_formatter = DateFormatter('%d')
    period = '%s - %s' % (from_date.strftime(date_format), to_date.strftime(date_format))

    fig, ax = plt.subplots(2, sharex=True, gridspec_kw={'height_ratios': [2, 1]})

    main_plot = ax[0]
    main_plot.set_xlabel('Price')
    main_plot.xaxis.set_label_position('top')
    main_plot.xaxis.set_major_locator(mondays)
    main_plot.xaxis.set_minor_locator(all_days)
    main_plot.xaxis.set_major_formatter(week_formatter)
    main_plot.xaxis.set_minor_formatter(day_formatter)
    main_plot.xaxis.grid(color='k', linewidth=.5, which='minor')
    main_plot.yaxis.grid(color='k', linewidth=.5, which='major')
    main_plot.yaxis.set_major_formatter(FuncFormatter(dollars))
    ohlc_tuples = [tuple(x) for x in data.values]
    candlestick_ohlc(main_plot, ohlc_tuples, width=0.5, colordown='r', colorup='g', alpha=.5)

    second_plot = ax[1]
    second_plot.set_xlabel('Volume')
    second_plot.xaxis.set_label_position('top')
    second_plot.yaxis.set_major_formatter(FuncFormatter(millions))
    second_plot.xaxis.grid(color='k', linewidth=.5, which='minor')
    second_plot.yaxis.grid(color='k', linewidth=.5, which='major')
    second_plot.get_xaxis().set_tick_params(direction='out', pad=15)
    second_plot.plot(data['Date'], data['Volume'])

    plt.tight_layout()

    fig.suptitle('%s (%s)' % (ticker, period), fontweight='bold')
    fig.subplots_adjust(top=0.90)

    folder_name = ResourceLocator.get_resource(ResourceType.OutFolder)
    file_name = folder_name + file
    if not os.path.isdir(folder_name):
        os.makedirs(folder_name)
    if os.path.isfile(file_name):
        os.remove(file_name)
    plt.savefig(file_name)

    return file_name


def main():
    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)
    email_section = config.config_section_map('NotificationEmail')

    file_name_1 = plot_and_save('1.png')
    file_name_2 = plot_and_save('2.png')

    email = Email(email_section)
    email.notify_message("test_body", "test", [file_name_1, file_name_2])

main()
