#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import subprocess
import time

from apscheduler.schedulers.background import BackgroundScheduler

from main.config.config import Config
from main.launch.job import Job
from main.tool.resource_locator import ResourceLocator, ResourceType


class Runner:

    def __init__(self):
        self.scheduler = BackgroundScheduler()
        self.jobs = []

    def load_jobs(self, jobs_config):
        # scheduler.add_job(self.tick, 'interval', seconds=5, args=[offline_mode])

        for section_name in jobs_config.sections():
            if section_name.find('Job') != -1:
                section = jobs_config.config_section_map(section_name)
                job = Job(section["name"], section["file_name"], section["params"],
                          section["cron_days"], section["cron_hour"], section["cron_minute"])
                self.jobs.append(job)

    def start_scheduler(self, offline_mode):
        file_name = './../reports/big_movement.py'
        params = ''

        # self.scheduler.add_job(self.run_script, 'interval', seconds=15, name="Big movement",
        #                        args=[offline_mode, file_name, params])
        #
        # self.run_script(offline_mode, file_name, params)
        #
        # self.scheduler.add_job(self.run_script, 'cron', day_of_week='mon-fri', hour='22', minute='0',
        #                        name="Big movement", args=[offline_mode, file_name, params])
        for job in self.jobs:
            self.scheduler.add_job(self.run_script, 'cron',
                                   day_of_week=job.cron_days, hour=job.cron_hour, minute=job.cron_minute,
                                   name=job.name, args=[offline_mode, job.file_name, job.name, job.params])

        self.scheduler.start()
        self.scheduler.print_jobs()
        # try:
        #     input_text = 'Press ENTER to exit or anything else to view jobs: '
        #     choice = input(input_text)
        #     while choice:
        #         self.scheduler.print_jobs()
        #         choice = input(input_text)
        # except (KeyboardInterrupt, SystemExit):
        #     pass
        try:
            while True:
                time.sleep(2)
        except (KeyboardInterrupt, SystemExit):
            self.scheduler.shutdown()

    @staticmethod
    def run_script(offline_mode, file_name, name, params=None):
        print()
        print("Run job \"%s\" (%s)" % (name, file_name))
        abs_file_name = os.path.abspath(file_name)
        dir_name = os.path.dirname(abs_file_name)
        file_name = os.path.basename(abs_file_name)
        try:
            subprocess.call(args=['python', file_name, 'offline_mode=' + str(offline_mode)], cwd=dir_name)
        except Exception as exception:
            print(exception)

        # file_name_without_ext = os.path.splitext(file_name)[0]
        # os.chdir(dir_name)
        # sub_process = subprocess.Popen(args=['python', '-m', file_name_without_ext], cwd=dir_name)
        # return sub_process.pid


def main():
    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)
    runner = Runner()

    jobs_config_path = ResourceLocator.get_resource(ResourceType.JobsFile)
    jobs_config = Config(jobs_config_path)
    runner.load_jobs(jobs_config)

    main_section = config.config_section_map("Main")
    offline_mode = int(main_section["offline_mode"])

    if __name__ == '__main__':
        runner.start_scheduler(offline_mode)

main()
