#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import logging.handlers

from apscheduler.schedulers.blocking import BlockingScheduler

from main.config.config import Config
from main.notification.email import Email
from main.notification.sms import SMS
from main.source.investing.inventory.loader import Loader as InventoryLoader
from main.source.investing.inventory.source import *
from main.tool.resource_locator import ResourceLocator, ResourceType


class News:

    def __init__(self, config):
        self.latest_events = []

        self.logger = logging.getLogger("main")
        self.__setup_logger()

        email_section = config.config_section_map('NotificationEmail')
        self.email_notifications = Email(email_section)

        sms_section = config.config_section_map('NotificationSMS')
        self.sms_notifications = SMS(sms_section)

    def __setup_logger(self):
        self.logger.setLevel(logging.DEBUG)

        default_suffix = "%Y-%m-%d"

        logs_path = '.\\logs\\'
        if not os.path.exists(logs_path):
            os.makedirs(logs_path)

        # all events into verbose.log
        fh_main = logging.handlers.TimedRotatingFileHandler(logs_path + 'verbose.log', when='midnight')
        fh_main.suffix = default_suffix
        fh_main.setLevel(logging.DEBUG)

        # new events into events.log
        fh_events = logging.handlers.TimedRotatingFileHandler(logs_path + 'events.log', when='midnight')
        fh_events.suffix = default_suffix
        fh_events.setLevel(logging.WARNING)

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh_main.setFormatter(formatter)
        fh_events.setFormatter(formatter)
        ch.setFormatter(formatter)

        self.logger.addHandler(fh_main)
        self.logger.addHandler(fh_events)
        self.logger.addHandler(ch)

    def start_scheduler(self, offline_mode):
        scheduler = BlockingScheduler()
        scheduler.add_job(self.tick, 'interval', seconds=5, args=[offline_mode])
        print('Press Ctrl+{0} to exit'.format('Break' if os.name == 'nt' else 'C'))

        try:
            scheduler.start()
        except (KeyboardInterrupt, SystemExit):
            pass

    def detect_news_events(self, data_sets, offline_mode):
        if len(self.latest_events) == 0:
            for data_set in data_sets:
                self.latest_events.append(data_set[0])

            return []
        else:
            new_events = []
            for index, data_set in enumerate(data_sets):
                if (offline_mode == 1) or (self.latest_events[index].event_id != data_set[0].event_id):
                    new_events.append(data_set[0])
                    self.latest_events[index] = data_set[0]

            return new_events

    def notify(self, news_event, event_level):
        if event_level >= logging.WARNING:
            sms_id = self.sms_notifications.notify_sms(news_event)
            if sms_id == self.sms_notifications.sms_default_id:
                self.email_notifications.notify(news_event)
        elif event_level >= logging.INFO:
            self.email_notifications.notify(news_event)

    def tick(self, offline_mode):
        data_sources = [Crude(), Gasoline(), API()]
        self.logger.info("--------------------")
        data_sets = []
        inventory_loader = InventoryLoader()
        for data_source in data_sources:

            data_set = inventory_loader.get_data(data_source, offline_mode, datetime.now())
            self.logger.info(data_source.name)
            self.logger.info(data_set)
            data_sets.append(data_set)

        news_events = self.detect_news_events(data_sets, offline_mode)
        for news_event in news_events:
            self.logger.warning("NEWS")
            self.logger.warning(news_event)
            self.notify(news_event, logging.WARNING)


def main():
    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)
    console = News(config)
    main_section = config.config_section_map("Main")
    offline_mode = int(main_section["offline_mode"])

    if __name__ == '__main__':
        console.start_scheduler(offline_mode)

main()
