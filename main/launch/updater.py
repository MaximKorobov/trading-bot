#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import os
import subprocess
import tempfile

import psutil
from dulwich import index
from dulwich.client import HttpGitClient
from dulwich.repo import Repo

from main.config.config import Config
from main.notification.sms import SMS
from main.tool.date import Date


# import shutil
from main.tool.resource_locator import ResourceLocator, ResourceType


class Updater:

    def __init__(self):
        config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
        config = Config(config_path)

        self.config = config

        sms_section = self.config.config_section_map('NotificationSMS')
        self.sms = SMS(sms_section)

        git_section = self.config.config_section_map('Git')
        self.git_url = git_section["url"]
        self.git_login = git_section["login"]
        self.git_password = git_section["password"]

        self.process_name = "python.exe"

    def stop_runner(self):
        """The idea is to kill all python.exe processes except for current process which is like a daemon"""
        current_pid = os.getpid()

        for process in psutil.process_iter():
            if process.name() == self.process_name:
                if process.pid != current_pid:
                    process.kill()

    def update_sources(self):
        folder_name = tempfile.gettempdir() + '/trading-bot'

        # Debug
        # if os.path.isdir(folder_name):
        #     shutil.rmtree(folder_name)

        if not os.path.isdir(folder_name):
            os.makedirs(folder_name)
            local_repo = Repo.init(folder_name, mkdir=False)
        else:
            local_repo = Repo(folder_name)

        remote_repo = HttpGitClient(self.git_url, username=self.git_login, password=self.git_password)
        remote_refs = remote_repo.fetch(self.git_url, local_repo)

        if local_repo[b"HEAD"].id.decode("utf-8") != remote_refs[b"refs/heads/master"].decode("utf-8"):
            local_repo[b"HEAD"] = remote_refs[b"refs/heads/master"]

            index_file = local_repo.index_path()
            tree = local_repo[b"HEAD"].tree
            index.build_index_from_tree(local_repo.path, index_file, local_repo.object_store, tree)

            message = local_repo[b"HEAD"].message.decode("utf-8")
            author = local_repo[b"HEAD"].author.decode("utf-8")
            commit_date = datetime.datetime.fromtimestamp(local_repo[b"HEAD"].commit_time)

            return "Trading bot update from %s at %s: %s" % (author, Date.date_time_str(commit_date), message)
        else:
            return None

    def launch_runner(self):
        sub_process = subprocess.Popen([self.process_name])
        return sub_process.pid

    def report_update(self, update_message):
        self.sms.notify_sms(update_message)

    def tick(self):
        self.stop_runner()

        update_message = self.update_sources()
        self.launch_runner()

        if update_message is not None:
            self.report_update(update_message)

u = Updater()
u.tick()
