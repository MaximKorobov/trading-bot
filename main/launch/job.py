#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Job:
    def __init__(self, name, file_name, params, cron_days, cron_hour, cron_minute):
        self.name = name
        self.file_name = file_name
        self.params = params
        self.cron_days = cron_days
        self.cron_hour = cron_hour
        self.cron_minute = cron_minute
