#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage


class Email:
    def __init__(self, section):
        self.from_address = section['from_address']
        self.to_address = section['to_address']
        self.smtp_server = section['smtp_server']
        self.username = section['username']
        self.password = section['password']

    def notify(self, news_event):
        subject = 'News event: %s (%s)' % (news_event.name, news_event.date)
        message = repr(news_event)

        message = Email.__encode_new_lines__(message)

        self.__send_simple__(subject, message)

    @staticmethod
    def __encode_new_lines__(text):
        return "<br />".join(text.split("\n"))

    def notify_message(self, message, subject, images=None):
        message = Email.__encode_new_lines__(message)

        if images:
            msg = MIMEMultipart('related')
            msg["From"] = self.from_address
            msg["To"] = self.to_address
            msg["Subject"] = subject

            # main text should be first
            msg_text = '<br>%s<br>' % message
            for file in images:
                head, file_name = os.path.split(file)
                msg_text += '<br><img src="cid:%s"><br><b>%s</b><br>' % (file_name, file_name)
            msg.attach(MIMEText(msg_text, 'html'))

            # embed images
            for file in images:
                head, file_name = os.path.split(file)

                fp = open(file, 'rb')
                img = MIMEImage(fp.read())
                fp.close()
                img.add_header('Content-ID', '<{}>'.format(file_name))
                img.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
                msg.attach(img)

            self.__send__(msg.as_string())
        else:
            self.__send_simple__(subject, message)

    def __send_simple__(self, subject, message):
        body = MIMEText(message, 'html', "utf-8")
        msg = MIMEMultipart("alternative")
        msg.attach(body)

        msg["From"] = self.from_address
        msg["To"] = self.to_address
        msg["Subject"] = subject

        self.__send__(msg.as_string().encode('utf-8'))

    def __send__(self, message):
        server = smtplib.SMTP(self.smtp_server)

        server.starttls()
        server.login(self.username, self.password)
        server.sendmail(self.from_address, self.to_address, message)
        server.quit()
