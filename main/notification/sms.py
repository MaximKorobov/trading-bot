#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import requests


class SMS:
    sms_default_id = 0

    def __init__(self, section):
        self.api_key = section['api_key']
        self.phone = section['phone']

    def notify_sms(self, news_event, sms_result_id=sms_default_id):
        sms_id = sms_result_id

        if isinstance(news_event, str):
            text = news_event
        else:
            text = repr(news_event)
        # Check SMS cost
        cost_url = 'http://sms.ru/sms/cost?api_id=%s&to=%s&text=%s' % (self.api_key, self.phone, text)

        cost_response = requests.get(cost_url)
        cost = 1
        amount = 0
        if cost_response.status_code == 200:
            content_line = str(cost_response.content, encoding='utf-8-sig').splitlines()
            if int(content_line[0]) == 100:
                cost = float(content_line[1])
                amount = int(content_line[2])
        else:
            print(cost_response.status_code)
            print(cost_response)

        # If everything is ok, send SMS
        if cost == 0 and amount == 1:
            send_url = 'http://sms.ru/sms/send?api_id=%s&to=%s&text=%s' % (self.api_key, self.phone, text)
            send_response = requests.get(send_url)
            if send_response.status_code == 200:
                content_line = str(send_response.content, encoding='utf-8-sig').splitlines()
                if int(content_line[0]) == 100:
                    sms_id = content_line[0 + amount]

        return sms_id
