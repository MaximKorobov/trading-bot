#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from main.source.investing import BaseLoader
from main.source.investing.index import Extractor, Parser


class Loader(BaseLoader):

    def get_service_url(self, source, start_date):
        return "https://www.investing.com/indices/Service/PriceInstrument?" + source.instrument_id

    def extract_data(self, data):
        self.is_have_more_history = False
        result = Extractor.get_table(data)

        return result

    def include_headers(self):
        return False

    def get_data_url(self, source, start_date, start_id=None):
        return None

    def parse_html(self, content, inventory, date):
        table_parser = Parser(inventory.name, date)
        table_parser.feed(content)

        result = table_parser.get_data()
        return result, None
