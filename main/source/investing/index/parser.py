#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from html.parser import HTMLParser
from main.source.investing.index import Data


class Parser(HTMLParser):
    def __init__(self, name, date):
        HTMLParser.__init__(self)

        self.date = date

        self.debugParsing = False

        self.name = name
        self.last = 0
        self.change = 0
        self.change_percent = 0
        self.volume = 0
        self.time = 0

        self.name_read = False
        self.name_a_read = False
        self.last_read = False
        self.change_read = False
        self.change_percent_read = False
        self.volume_read = False
        self.time_read = False

        self.items = []

    @staticmethod
    def clean_data(data):
        result = data

        result = result.replace("\n", "")
        result = result.replace("&nbsp;", "")
        if result.find("/") == 0:
            result = result.replace("/", "", 1)
        result = result.replace("%", "")
        result = result.replace("+", "")
        result = result.replace(" ", "")  # %nbsp; in ASCII charset
        result = result.replace(" ", "")

        return result

    @staticmethod
    def convert_value(value, default_value=None):
        result = Parser.clean_data(value)

        if result == '' or result is None:
            result = default_value
        else:
            if len(result) > 0 and result[-1] == "M":
                result = round(float(result[:-1]) * 1000000)
            elif len(result) > 0 and result[-1] == "K":
                result = round(float(result[:-1]) * 1000)
            else:
                result = int(result)

        return result

    def get_data(self):
        return self.items

    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        if tag == 'td':
            if self.debugParsing:
                print("New of td: ")

            for name, value in attrs:
                if name == "class":
                    if value.find("bold left noWrap elp") != -1:
                        self.name_read = True
                    elif value.find("-last") != -1:
                        self.last_read = True
                    elif value.find("-pcp") != -1:
                        self.change_percent_read = True
                    elif value.find("-pc") != -1:
                        self.change_read = True
                    elif value.find("-turnover") != -1:
                        self.volume_read = True
                    elif value.find("-time") != -1:
                        self.time_read = True

        if self.name_read and tag == 'a':
            self.name_a_read = True

    def handle_data(self, data):
        if self.debugParsing:
            print("Data: " + data)

        clean_data = self.clean_data(data)

        if self.name_a_read:
            self.name = data
            self.name_a_read = False
            self.name_read = False

        if self.last_read:
            self.last = clean_data
            self.last_read = False

        if self.change_read:
            self.change = clean_data
            self.change_read = False

        if self.change_percent_read:
            self.change_percent = clean_data
            self.change_percent_read = False

        if self.volume_read:
            self.volume = Parser.convert_value(data)
            self.volume_read = False

        if self.time_read:
            self.time = clean_data
            self.time_read = False

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self.debugParsing:
                print("End of td: ")

            item = Data(self.name, self.last, self.change,
                        self.change_percent, self.volume,
                        self.time)

            self.items.append(item)
