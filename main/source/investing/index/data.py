#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Data:
    def __init__(
            self,
            name,
            last,
            change,
            change_percent,
            volume,
            time
    ):
        self.name = name

        self.last = last
        self.change = change
        self.change_percent = change_percent
        self.volume = volume
        self.time = time

    def __repr__(self):
        return "Name: %s; Change: %s; Change %%: %s; Volume: %s; Time: %s" % (
            self.name,
            self.change, self.change_percent,
            self.volume, self.time
        )
