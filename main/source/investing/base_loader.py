#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta, abstractmethod
from datetime import datetime
import requests

from main.tool.cache import BlobCache
from main.tool.resource_locator import ResourceLocator, ResourceType


class BaseLoader(metaclass=ABCMeta):

    def __init__(self):
        self.is_have_more_history = None
        self.use_one_day_cache = True

    def get_multiple_data(self, sources, offline_mode, start_date=datetime.now()):
        result = []
        for source in sources:
            result.append(self.get_data(source, offline_mode, start_date))
        return result

    def get_data(self, source, offline_mode, start_date=datetime.now(), start_id=None):
        if offline_mode:
            return self.get_offline_data(source, start_date)
        else:
            data = None
            folder = ResourceLocator.get_resource(ResourceType.OutFolder)

            if self.use_one_day_cache:
                data = BlobCache.load_history(source.name, start_date, datetime.now(), folder)

            if not data:
                data, hasMoreHistory = self.get_real_data(source, start_date)
                if self.use_one_day_cache:
                    BlobCache.save_history(data, source.name, start_date, datetime.now(), folder)

            return data

    def get_offline_data(self, source, start_date):
        file = open(source.get_offline_data_abs_file_name())
        data = file.read()
        file.close()
        data = self.parse_html(data, source, start_date)

        return data

    def get_real_data(self, source, start_date=datetime.now(), start_id=None):
        # Direct is not working now
        headers = {
            'X-Requested-With': 'XMLHttpRequest',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0'
        }
        if self.include_headers():
            headers['Content-Type'] = 'application/x-www-form-urlencoded'

        url = self.get_service_url(source, start_date)
        url_params = self.get_data_url(source, start_date, start_id)
        response = requests.post(url, url_params, headers=headers)

        data = ""
        more_history = False
        if response.status_code == 200:
            data = str(response.content, encoding='utf-8-sig')

            data = self.extract_data(data)
            data, start_id = self.parse_html(data, source, start_date)

            more_data = []
            if self.is_have_more_history and start_id:
                more_data, more_history = self.get_real_data(source, start_date, start_id)
            if more_data:
                data.extend(more_data)

        return data, more_history

    @abstractmethod
    def get_service_url(self, source, start_date):
        pass

    @abstractmethod
    def extract_data(self, data):
        pass

    @abstractmethod
    def include_headers(self):
        pass

    @abstractmethod
    def get_data_url(self, source, start_date, start_id=None):
        pass

    @abstractmethod
    def parse_html(self, data, inventory, start_date):
        return None, None
