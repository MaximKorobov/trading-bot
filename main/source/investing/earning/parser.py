#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
from html.parser import HTMLParser
from main.source.investing.earning import Data


class Parser(HTMLParser):

    def __init__(self, name, date):
        HTMLParser.__init__(self)

        self.name = name
        self.date = date

        self.debugParsing = False
        self.tdRead = False
        self.tdIndex = 0

        self.event_id = 0
        self.timestamp = 0
        self.period_end = ""
        self.eps_actual = ""
        self.eps_forecast = ""
        self.revenue_actual = ""
        self.revenue_forecast = ""

        self.items = []

    @staticmethod
    def clean_data(data):
        result = data

        result = result.replace("\n", "")
        result = result.replace("&nbsp;", "")
        if result.find("/") == 0:
            result = result.replace("/", "", 1)
        result = result.replace(" ", "")  # %nbsp; in ASCII charset
        result = result.replace(" ", "")

        return result

    def get_data(self):
        return self.items

    def error(self, message):
        pass

    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            if self.debugParsing:
                print("New of td: ")

            for name, value in attrs:
                if name == 'id':
                    pass  # There is no id field in earning
                if name == 'event_timestamp':
                    self.timestamp = datetime.datetime.strptime(value, '%Y-%m-%d')
                    self.event_id = value

        if tag == 'td':
            self.tdIndex += 1
            self.tdRead = False

    def handle_data(self, data):
        if self.debugParsing:
            print("Data: " + data)

        clean_data = self.clean_data(data)

        if not self.tdRead:
            if self.tdIndex == 2:
                self.period_end = clean_data
            elif self.tdIndex == 3:
                self.eps_actual = clean_data
            elif self.tdIndex == 4:
                self.eps_forecast = clean_data
            elif self.tdIndex == 5:
                self.revenue_actual = clean_data
            elif self.tdIndex == 6:
                self.revenue_forecast = clean_data
            self.tdRead = True

    def handle_endtag(self, tag):
        if tag == 'tr':
            if self.debugParsing:
                print("End of td: ")

            self.tdIndex = 0

            item = Data(self.event_id, self.name, self.timestamp, self.period_end,
                        self.eps_actual, self.eps_forecast,
                        self.revenue_actual, self.revenue_forecast)
            self.items.append(item)
