#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Data:
    def __init__(
            self,
            event_id,
            name,
            release_date,
            period_end,
            eps_actual,
            eps_forecast,
            revenue_actual,
            revenue_forecast
    ):
        self.event_id = event_id
        self.name = name

        self.release_date = release_date
        self.period_end = period_end

        self.eps_actual = eps_actual
        self.eps_forecast = eps_forecast
        self.revenue_actual = revenue_actual
        self.revenue_forecast = revenue_forecast

    def __repr__(self):
        return "Id: %s; Name: %s; Date/Period: %s / %s; EPS: %s / %s; Revenue: %s / %s" % (
            self.event_id, self.name,
            self.release_date, self.period_end,
            self.eps_actual, self.eps_forecast,
            self.revenue_actual, self.revenue_forecast
        )
