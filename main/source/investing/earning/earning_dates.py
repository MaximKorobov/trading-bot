#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta

import datetime as dt

from main.source.investing.earning import Loader
from main.tool.cache.time_cache import TimeCache
from main.tool.resource_locator import ResourceLocator, ResourceType


class EarningDates(metaclass=ABCMeta):
    def __init__(self, offline_mode, earning, cache_days=0, skip_before=0, skip_after=0):
        self.offline_mode = offline_mode
        self.earning = earning
        self.cache_days = cache_days
        self.skip_before = skip_before
        self.skip_after = skip_after
        self.loader = Loader()

    def get_skip_dates(self, start_date=dt.datetime.now()):
        dates = []

        abs_folder_name = ResourceLocator.get_resource(ResourceType.OutFolder)
        save_date = dt.datetime.now() - dt.timedelta(days=self.cache_days)
        ticker = self.earning.ticker

        earning_infos = TimeCache.load_history(ticker, save_date, abs_folder_name)
        if not earning_infos:
            earning_infos = self.loader.get_data(self.earning, self.offline_mode, start_date)

            if self.cache_days != 0:
                TimeCache.save_history(earning_infos, ticker, abs_folder_name)

        earning_dates = [earning_info.release_date for earning_info in earning_infos]

        for date in earning_dates:
            # Similar to "skip_after_days", but some signs are reversed to preserve calendar order from earlier to later
            for skip_before_days in range(-self.skip_before, -1):
                value = date + dt.timedelta(days=skip_before_days + 1)
                dates.append(value.date())

            value = date
            dates.append(value.date())

            for skip_after_days in range(0, self.skip_after):
                value = date + dt.timedelta(days=skip_after_days + 1)
                dates.append(value.date())

        dates = sorted(dates)  # from 0 to 1
        return dates
