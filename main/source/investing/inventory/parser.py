#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
from html.parser import HTMLParser
from main.source.investing.inventory.data import Data


class Parser(HTMLParser):
    def __init__(self, name, date):
        HTMLParser.__init__(self)

        self.name = name
        self.date = date

        self.debugParsing = False
        self.newRow = False
        self.newSpan = False
        self.newNoWrap = False
        self.newBlackNoWrap = False
        self.tdIndex = 0

        self.event_id = 0
        self.timestamp = 0
        self.actual = 0
        self.forecast = 0
        self.previous = 0

        self.items = []

    def error(self, message):
        pass

    @staticmethod
    def clean_data(data):
        result = data

        result = result.replace("\n", "")
        result = result.replace("&nbsp;", "")
        if result.find("/") == 0:
            result = result.replace("/", "", 1)
        result = result.replace(" ", "")  # %nbsp; in ASCII charset
        result = result.replace(" ", "")

        return result

    @staticmethod
    def convert_value(value, default_value=None):
        result = Parser.clean_data(value)

        if result is None or result.strip() == '':
            result = default_value
        else:
            if len(result) > 0 and result[-1] == "M":
                result = round(float(result[:-1]) * 1000000)
            else:
                result = int(result)

        return result

    def handle_starttag(self, tag, attrs):
        if tag == 'tr':
            if self.debugParsing:
                print("New of td: ")

            self.newRow = True
            for name, value in attrs:
                if name == 'id':
                    self.event_id = value
                if name == 'event_timestamp':
                    self.timestamp = datetime.datetime.strptime(value, '%Y-%m-%d %H:%M:%S')

        if tag == 'td' and self.newRow:
            for name, value in attrs:
                if name == 'class':
                    if value == 'noWrap':
                        self.newNoWrap = True
                    elif value == 'blackFont noWrap':
                        self.newBlackNoWrap = True

        if tag == 'span' and self.newRow:
            self.newSpan = True

    def handle_data(self, data):
        if self.debugParsing:
            print("Data: " + data)

        if self.newSpan:
            self.actual = self.convert_value(data, 0)
        elif self.newNoWrap:
            self.forecast = self.convert_value(data, 0)
        elif self.newBlackNoWrap:
            self.previous = self.convert_value(data, 0)

    def handle_endtag(self, tag):
        if tag == 'span':
            self.newSpan = False

        if tag == 'td':
            self.newNoWrap = False
            self.newBlackNoWrap = False

        if tag == 'tr':
            if self.debugParsing:
                print("End of td: ")

            self.newRow = False
            self.tdIndex = 0

            item = Data(self.event_id, self.name, self.timestamp, self.actual, self.forecast, self.previous)
            self.items.append(item)

    def get_data(self):
        return self.items

