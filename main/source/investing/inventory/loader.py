#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json

import datetime

from main.source.investing.base_loader import BaseLoader
from main.source.investing.inventory.parser import Parser


class Loader(BaseLoader):

    def get_service_url(self, source, start_date):
        return "https://www.investing.com/economic-calendar/more-history"

    def extract_data(self, data):
        self.is_have_more_history = json.loads(data)['hasMoreHistory']
        return json.loads(data)['historyRows']

    def include_headers(self):
        return True

    def get_data_url(self, source, start_date, start_id=None):
        date = start_id if start_id else datetime.datetime.now()

        string_date = "%04d-%02d-%02d+%02d:%02d:%02d" % (
            date.year, date.month, date.day,
            date.hour, date.minute, date.second
        )
        url = source.instrument_id + "&event_timestamp=" + string_date + "&is_speech=0"
        # url = source.instrument_id + "&eventID=" + (start_id or "") + "&is_speech=0"
        return url

    def parse_html(self, content, inventory, date):

        # Data
        table_parser = Parser(inventory.name, date)
        table_parser.feed(content)
        result = table_parser.get_data()

        # Start id
        start_id = None
        if self.is_have_more_history:
            if result and len(result) > 0:
                oldest_data = result[len(result) - 1]
                if oldest_data.date > date:
                    start_id = oldest_data.date

        return result, start_id
