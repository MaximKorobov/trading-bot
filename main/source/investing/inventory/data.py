#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Data:

    def __init__(
            self,
            event_id,
            name,
            date,
            actual,
            forecast,
            previous
    ):
        self.event_id = event_id
        self.name = name
        self.date = date
        self.forecast = forecast
        self.actual = actual
        self.previous = previous
        self.change = self.actual - self.previous

    def __repr__(self):
        return "Id: {0}; Name: {1}; Date: {2}; Actual: {3}; Forecast {4}; Forecast {5}".format(
            self.event_id, self.name, self.date, self.actual, self.forecast, self.previous)

