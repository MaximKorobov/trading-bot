#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
from datetime import datetime


class Inventory:

    @staticmethod
    def get_base_demo_path():
        return "./../source/investing/inventory/demo/"

    def __init__(self):
        self.name = ""
        self.instrument_id = ""
        self.demo_file_name = ""

    def set_data(self, name, instrument_id, demo_file_name):
        self.name = name
        self.instrument_id = instrument_id
        self.demo_file_name = demo_file_name

    def get_offline_data_abs_file_name(self):
        file_name = self.get_base_demo_path() + self.demo_file_name
        abs_file_name = os.path.abspath(file_name)
        return abs_file_name

    def __repr__(self):
        return self.name
