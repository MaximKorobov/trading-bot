#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from main.source.investing.inventory.source.inventory import Inventory


class RigsCount(Inventory):

    def __init__(self):
        super().__init__()
        super().set_data("U.S. Baker Hughes Oil Rig Count", "eventID=0&event_attr_ID=1652", "rigs_count.html")

