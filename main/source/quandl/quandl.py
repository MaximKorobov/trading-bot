#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import quandl
from main.tool.cache import BlobCache
from main.tool.resource_locator import ResourceLocator, ResourceType


class Quandl:

    def __init__(self, api_key, company, use_cache=True):
        self.company = company
        self.ticker = company.get_full_ticker(True)
        self.use_cache = use_cache
        quandl.ApiConfig.api_key = api_key

    def get_history(self, from_date, to_date):

        data = None
        folder = ResourceLocator.get_resource(ResourceType.OutFolder)

        if self.use_cache:
            data = BlobCache.load_history(self.ticker, from_date, to_date, folder)

        if data is None or data.empty:
            data = quandl.get(dataset=self.ticker, start_date=from_date, end_date=to_date)
            if self.use_cache:
                BlobCache.save_history(data, self.ticker, from_date, to_date, folder)

        return data
