#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pandas
import yahoo_finance as yf

from main.tool.cache.blob_cache import BlobCache
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType


class Share:

    def __init__(self, ticker):
        self.ticker = ticker
        self._share = None

    @property
    def share(self):
        if not self._share:
            self._share = yf.Share(self.ticker)

        return self._share

    def get_history(self, from_date, to_date):
        from_date_str, to_date_str = Date.dates_str(from_date, to_date)
        history = self.share.get_historical(from_date_str, to_date_str)

        return history

    def load_data(self, from_date, to_date, keep_index=None):
        folder = ResourceLocator.get_resource(ResourceType.OutFolder)

        data = BlobCache.load_history(self.ticker, from_date, to_date, folder)
        if not data:
            data = self.get_history(from_date, to_date)
            BlobCache.save_history(data, self.ticker, from_date, to_date, folder)
        if keep_index:
            df = pandas.DataFrame.from_records(reversed(data), index='Date', columns=['Date', 'Close'])
            # df = pandas.DataFrame.from_records(reversed(data), columns=['Date', 'Close'])
            # # df['Date'] = 0#pandas.to_datetime(df['Date'])
            # df.index = pandas.DatetimeIndex(df['Date'], freq='M')
        else:
            df = pandas.DataFrame.from_records(reversed(data), index='Date', columns=['Date', 'Close'])
            df = df.astype(float)
            df = df.rename(columns={'Close': self.ticker})

        return df
