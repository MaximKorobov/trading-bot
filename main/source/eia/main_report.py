#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import pandas


class EIACrudeOil:

    @staticmethod
    def get_data():
        # Latest result from the source: http://www.eia.gov/petroleum/supply/weekly/
        data_frame = pandas.read_csv("http://ir.eia.gov/wpsr/table1.csv", index_col=False, error_bad_lines=False)

        crude_data = data_frame.values[1, 3]
        gasoline_data = data_frame.values[3, 3]

        # return content
        return crude_data, gasoline_data
