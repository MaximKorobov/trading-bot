#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt


class Remote:

    @staticmethod
    def _get_data_(config, dataset, ticker, from_date, to_date):
        quandl_section = config.config_section_map('SourceQuandl')
        quandl_api_key = quandl_section['api_key']

        data = bt.feeds.Quandl(dataname=ticker, period='h', reverse=True, fromdate=from_date, todate=to_date,
                               apikey=quandl_api_key, dataset=dataset, adjclose=False)

        return data

    @staticmethod
    def get_data(config, company, from_date, to_date):
        full_ticker = company.get_full_ticker()
        data = Remote._get_data_(config, company.database, full_ticker, from_date, to_date)

        return data
