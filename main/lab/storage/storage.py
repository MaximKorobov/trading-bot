#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta
import datetime


class Storage(metaclass=ABCMeta):

    def __init__(self):
        self.orders = []
        self.most_recent_order = None

    def get_most_recent_order(self):
        return self.most_recent_order

    def __find_most_recent_order__(self):
        result = None

        if self.orders:
            for order in self.orders:
                if result is None or order.date > result:
                    result = order.date

        return result

    def get_order(self, date):
        result = None

        for order in self.orders:
            if datetime.datetime.date(order.date) == datetime.datetime.date(date):
                result = order

        return result
