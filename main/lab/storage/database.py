#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os

import pony.orm as orm
import datetime

from main.lab.storage.storage import Storage
from main.tool.resource_locator import ResourceLocator, ResourceType

db = orm.Database()


class DatabaseOrder(db.Entity):
    id = orm.PrimaryKey(int, auto=True)

    orders_key = orm.Required(str)

    ticker = orm.Required(str)
    lot = orm.Required(int)

    date = orm.Required(datetime.datetime)
    price = orm.Required(float)


class Database(Storage):

    class Mode:
        ReadOnly, ReadWrite = range(2)

    @staticmethod
    def demo_order_key():
        return "demo_orders"

    @orm.db_session
    def load_orders(self):
        return list(DatabaseOrder.select(lambda o: o.orders_key == self.orders_key))

    @orm.db_session
    def set_order(self, date, lot, price, ticker):
        DatabaseOrder(orders_key=self.orders_key, date=date, lot=lot, price=price, ticker=ticker)

        orm.commit()

    def create_demo_orders(self):
        self.set_order(date=datetime.datetime(2016, 1, 25), lot=10, price=100, ticker="NFLX")
        self.set_order(date=datetime.datetime(2016, 6, 1), lot=10, price=120, ticker="NFLX")

    def __init__(self, orders_key, mode=Mode.ReadOnly):
        super().__init__()

        should_add_test_data = orders_key == Database.demo_order_key()

        self.orders_key = orders_key

        abs_folder_name = ResourceLocator.get_resource(ResourceType.OutFolder)
        file_name = 'orders.sqlite'
        abs_file_name = os.path.abspath(abs_folder_name + file_name)

        if os.path.exists(abs_file_name) and should_add_test_data:
            os.remove(abs_file_name)

        db.bind('sqlite', abs_file_name, create_db=True)
        db.generate_mapping(create_tables=True)

        if should_add_test_data:
            self.create_demo_orders()

        self.orders = self.load_orders()
        self.most_recent_order = self.__find_most_recent_order__()

        if mode == Database.Mode.ReadOnly:
            db.disconnect()

