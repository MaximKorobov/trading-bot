#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Index(object):

    def __init__(self):
        self.name = None
        self.description = None
        self.store_key = None
        self.components_source = None

        self.database = None
        self.dataset = None
        self.ticker = None

    def get_store_key(self):
        return self.store_key if self.store_key else self.ticker

    def get_full_ticker(self, include_database=False):
        result = self.dataset + '_' + self.ticker

        if include_database:
            result = self.database + "/" + result

        return result
