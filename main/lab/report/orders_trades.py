#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from backtrader.utils import num2dt
from tabulate import tabulate

from main.tool.date import Date


class OrdersTrades:
    @staticmethod
    def print_strategy_orders(strategy):
        def get_row_data(order):
            if order.executed.size == 0:
                order_type = "SKIP"
            else:
                order_type = "BUY" if order.isbuy() else "SELL"

            return num2dt(order.executed.dt), order_type, order.executed.size, order.executed.price, \
                order.executed.value

        def filter_order(order):
            if order.status in [order.Completed, order.Canceled, order.Margin]:
                return True
            else:
                return False

        orders = strategy._orders.copy()
        orders = list(filter(filter_order, orders))
        table_data = list(map(get_row_data, orders))

        print()
        print("Orders:")
        print(tabulate(table_data, headers=["Date", "Action", "Size", "Price", "Cost"]))

    @staticmethod
    def print_orders(orders, last_close_price=None, last_close_date=None):
        def get_row_data(order):
            open_order = order[0]

            close_order = order[1] if len(order) > 1 else None
            close_price = None
            close_date = None

            if close_order:
                close_price = close_order[1][0][1]
                close_date = close_order[0]
            elif last_close_price and last_close_date:
                close_price = last_close_price
                close_date = num2dt(last_close_date)

            lot = open_order[1][0][0]
            is_long = lot > 0

            if close_price and close_date:
                open_price = open_order[1][0][1]

                profit = (close_price - open_price) * lot
                profit_str = "%.2f" % profit
                percents = "%.2f" % (profit / open_price * 100 / lot * (1 if is_long else -1))

                return Date.date_str(open_order[0]), open_price, lot, \
                    Date.date_str(close_date), close_price, profit_str, percents
            else:
                return Date.date_str(open_order[0]), open_order[1][0][1], lot, \
                    "now", "?", "?", "?"

        def group(lst, groups):  # http://stackoverflow.com/a/1624993/548395
            return [lst[i:i + groups] for i in range(0, len(lst), groups)]

        order_groups = list(group(orders, 2))
        table_data = list(map(get_row_data, order_groups))
        print()
        print("Orders:")
        print(tabulate(table_data,
                       headers=["Open", "Price", "Lot", "Close", "Price", "Profit", "%"],
                       showindex=True, tablefmt="simple", floatfmt=".2f"))
