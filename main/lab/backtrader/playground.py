#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime

from backtrader.analyzers import (SQN, AnnualReturn, TimeReturn, TradeAnalyzer, SharpeRatio_A, Transactions, DrawDown)

from main.config.config import Config
from main.lab.data.local import Local
from main.lab.data.remote import Remote
from main.lab.profile.company.netflix import Netflix
from main.lab.report.orders_trades import OrdersTrades
from main.lab.sizer.percents_sizer import AllInSizer
from main.lab.strategy import Base
from main.lab.strategy.ml.classification import *
from main.lab.strategy.ml.regression import *
from main.lab.tools.plotter import Plotter
from main.lab.tools.warmup import WarmupDetector
from main.tool.date import Date
from main.tool.hardware import Hardware
from main.tool.resource_locator import ResourceLocator, ResourceType


class RunMode:
    Live, DevelopIdea, Optimize, BackTestPeriods = range(4)


def run():
    # Parameters
    #
    #   Global
    #
    company = Netflix()
    #
    ticker = company.ticker
    local_data = False
    run_mode = RunMode.DevelopIdea
    orders_storage = None  # orders_storage = OrderStorage(OrderStorage.demo_order_key())
    #
    #   Time frame
    #
    periods = [
        Date.since_year(2016),
        Date.years_dates(2010, 2015),
        Date.years_dates(2013, 2014),
        Date.year_dates(2014),
        Date.last_months(6)
    ]
    test_percents = 80
    warm_up_indicators = False
    #
    #   Earning
    #
    skip_earning = False
    earning = company.earnings_source
    skip_earning_days = [1, 2]
    #
    #   Logs and graphics
    #
    silent_strategy = True
    silent_ml = False
    show_plot = True

    # Settings
    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)
    main_section = config.config_section_map("Main")
    offline_mode = int(main_section["offline_mode"])

    # Run
    now = datetime.datetime.now()
    if run_mode == RunMode.Live:
        # TODO:
        # 1. Load portfolio
        # 2. Predict action for today/tomorrow
        # 3. Report
        pass

    elif run_mode == RunMode.DevelopIdea or run_mode == RunMode.BackTestPeriods:

        periods_actual = (periods[0],)
        if run_mode == RunMode.BackTestPeriods:
            periods_actual = periods
            # Silence!
            silent_strategy = True
            silent_ml = True

        for period in periods_actual:
            back_trader = bt.Cerebro()

            # Dates
            from_date, to_date = period
            test_date = Date.dates_delta(from_date, to_date, test_percents)
            warm_up_period, warm_up_strategy_name = \
                add_strategies(back_trader, offline_mode, orders_storage, silent_ml, silent_strategy,
                               test_date, warm_up_indicators,
                               earning if skip_earning else None, skip_earning_days)
            if warm_up_indicators and warm_up_period:
                from_date = from_date - bt.datetime.timedelta(days=warm_up_period)

            # Data
            setup_backtrader(back_trader)

            data = get_data(config, company, local_data, from_date, to_date)
            back_trader.adddata(data)

            strategies = run_strategy(back_trader, run_mode == RunMode.DevelopIdea, from_date, to_date)

            if show_plot:
                if isinstance(strategies[0], BaseML):
                    plot(ticker, back_trader, strategies, test_date=test_date)
                else:
                    plot(ticker, back_trader, strategies)

    elif run_mode == RunMode.Optimize:

        back_trader = bt.Cerebro()
        from_date, to_date = periods[0]
        test_date = Date.dates_delta(from_date, to_date, test_percents)
        # back_trader.optstrategy(SMACross, sma1=range(1, 5), sma2=range(5, 11), silent=silent_strategy)
        # back_trader.optstrategy(TDW, buy_day=range(0, 4), sell_day=range(0, 4), silent=silent_strategy)
        back_trader.optstrategy(OneClass, knn=range(1, 15), test_date=test_date, silent=silent_strategy,
                                silent_ml=silent_ml)
        back_trader.addanalyzer(AnnualReturn)

        setup_backtrader(back_trader)

        data = get_data(config, company, local_data, from_date, to_date)
        back_trader.adddata(data)

        strategies = back_trader.run()
        best_params = None

        for index, strategy in enumerate(strategies):
            strategy_result = (strategy[0].analyzers.annualreturn.rets[0], str(strategy[0].p._getkwargs()))
            print(strategy_result)

            if not best_params:
                best_params = strategy_result
            elif strategy_result[0] > best_params[0]:
                best_params = strategy_result
        print("Best annual return (%s) with params:" % best_params[0])
        print(best_params[1])
    print("==================================")
    print()
    print("Platform:")
    print("  - " + Hardware.status())
    print("  - Time: %s seconds" % (datetime.datetime.now() - now).seconds)


def add_strategies(back_trader, offline_mode, orders_storage, silent_ml, silent_strategy, test_date,
                   warm_up_indicators,
                   earning, earning_skip_days):
    strategies_to_add = [MLP]
    # strategies_to_add = [RSISimple, RSIBuySell, SMA_RSI, SMACross, HolyGrail, MACD_ADX, BBands, SimilarBars]
    # strategies_to_add = [OneClass, VotingClass]
    # strategies_to_add = [TDW, MorningBuy]
    # strategies_to_add = [Linear, MLP]
    for strategy_to_add in strategies_to_add:
        if issubclass(strategy_to_add, BaseML):
            back_trader.addstrategy(strategy_to_add, offline_mode=offline_mode,
                                    silent=silent_strategy, orders_storage=orders_storage,
                                    test_date=test_date, silent_ml=silent_ml,
                                    earning=earning, earning_skip_before=earning_skip_days[0],
                                    earning_skip_after=earning_skip_days[1])
        else:
            back_trader.addstrategy(strategy_to_add, offline_mode=offline_mode,
                                    silent=silent_strategy, orders_storage=orders_storage,
                                    earning=earning, earning_skip_before=earning_skip_days[0],
                                    earning_skip_after=earning_skip_days[1])

    warm_up_period, warm_up_strategy_name = None, None
    if warm_up_indicators:
        warm_up_period, warm_up_strategy_name = WarmupDetector.detect(strategies_to_add)
    return warm_up_period, warm_up_strategy_name


def setup_backtrader(back_trader):
    # Add analyzers
    back_trader.addanalyzer(DrawDown)
    back_trader.addanalyzer(AnnualReturn)
    back_trader.addanalyzer(SharpeRatio_A, timeframe=bt.TimeFrame.Days, _name='sharpe')
    back_trader.addanalyzer(SQN, _name='sqn')
    back_trader.addanalyzer(TimeReturn, timeframe=bt.TimeFrame.Days)
    back_trader.addanalyzer(TradeAnalyzer)
    back_trader.addanalyzer(Transactions)

    back_trader.broker.setcash(1000.0)
    # back_trader.addsizer(bt.sizers.FixedSize, stake=10)
    back_trader.addsizer(AllInSizer)
    back_trader.broker.setcommission(commission=0.0)

    back_trader.params.maxcpus = 1


def get_data(config, company, local_data, from_date, to_date):
    if local_data:
        data_path = './source/yahoo/demo/netflix-2016.csv'
        data = Local.get_data(data_path, from_date, to_date)
    else:
        data = Remote.get_data(config, company, from_date, to_date)

    return data


def run_strategy(back_trader, verbose=True, from_date=None, to_date=None):
    start_value = back_trader.broker.getvalue()
    strategies = back_trader.run()
    if verbose:
        print('Starting Portfolio Value: %.2f' % start_value)
        print('\nAnalysis:')
    for strategy in strategies:
        separator_length = 0
        print()
        print('==================================')
        if verbose:
            print('Strategy:', strategy.__class__.__name__)
            if isinstance(strategy, Base):
                strategy_info = strategy.strategy_info()
                if strategy_info:
                    print("Description: %s" % strategy_info)
        else:
            print()
            print("Dates period: %s - %s (%s days)" %
                  (Date.date_str(from_date), Date.date_str(to_date), (to_date - from_date).days)
                  )

        strategy.analyzers.annualreturn.print(seplen=separator_length)
        if verbose:
            # http://www.investopedia.com/articles/07/sharpe_ratio.asp
            # http://www.morningstar.com/InvGlossary/sharpe_ratio.aspx
            strategy.analyzers.sharpe.print(seplen=separator_length)
            # http://www.quantshare.com/item-1541-system-quality-number-indicator
            strategy.analyzers.sqn.print(seplen=separator_length)
            # strategy.analyzers.drawdown.print(seplen=separator_length)
            # strategy.analyzers.tradeanalyzer.print(seplen=separator_length)
            # strategy.analyzers.timereturn.print(seplen=separator_length)
            # strategy.analyzers.transactions.print(seplen=separator_length)

            last_close_price = strategy.datas[0].close[0]
            last_close_date = strategy.datas[0].datetime[0]
            OrdersTrades.print_orders(list(strategy.analyzers.transactions.rets.items()),
                                      last_close_price, last_close_date)
    if verbose:
        print('==================================')
    else:
        print()

    end_value = back_trader.broker.getvalue()
    percents = (end_value - start_value) / start_value * 100
    print('Final portfolio value: %.2f (%.2f %%)' % (end_value, percents))
    return strategies


def plot(ticker, back_trader, strategies, test_date=0):
    titles = []
    for strategy in strategies:
        title = "%s - %s" % (ticker, strategy.__class__.__name__)
        if test_date != 0:
            title += " (%s)" % Date.date_str(test_date)
        titles.append(title)
    plotter = Plotter(titles)
    back_trader.plot(plotter=plotter, start=test_date)


if __name__ == '__main__':
    run()
