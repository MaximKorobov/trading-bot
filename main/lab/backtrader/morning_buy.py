#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
from backtrader.analyzers import (SQN, AnnualReturn, TimeReturn, TradeAnalyzer, SharpeRatio_A, Transactions)
from tabulate import tabulate

from main.config.config import Config
from main.lab.report.orders_trades import OrdersTrades
from main.lab.sizer.percents_sizer import AllInSizer
from main.lab.strategy.calendar.morning_buy import MorningBuy, Base
from main.lab.tools.plotter import Plotter
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType


def optimization_callback(test):
    global opt_results
    ar = test[0].analyzers.annualreturn.rets[0] if len(test[0].analyzers.annualreturn.rets) > 0 else 0
    opt_results.append((ar,
                        test[0].params.open_hour,
                        test[0].params.close_hour))


def setup(b_trader):
    b_trader.adddata(data)
    b_trader.broker.setcash(1000.0)
    b_trader.addsizer(AllInSizer)
    b_trader.broker.setcommission(commission=0.0)

if __name__ == '__main__':

    # ticker = 'BRENT'
    ticker = 'WTI'
    from_date, to_date = Date.last_months(2)
    local_data = 0

    config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
    config = Config(config_path)

    data = bt.feeds.GenericCSVData(
        dataname='./../../source/broker/demo/' + ticker + '60.csv',
        fromdate=from_date,
        todate=to_date,
        reverse=True,
        dtformat='%Y.%m.%d',
        tmformat='%H:%M',
        datetime=0,
        time=1,
        open=2,
        high=3,
        low=4,
        close=5,
        volume=6,
    )

    run = 1
    if run:
        silent_strategy = True
        back_trader = bt.Cerebro()

        back_trader.addanalyzer(SharpeRatio_A, timeframe=bt.TimeFrame.Days, _name='sharpe')
        back_trader.addanalyzer(SQN, _name='sqn')
        back_trader.addanalyzer(TimeReturn, timeframe=bt.TimeFrame.Days)
        back_trader.addanalyzer(TimeReturn, timeframe=bt.TimeFrame.Years, _name='annualreturn')
        back_trader.addanalyzer(TradeAnalyzer)
        back_trader.addanalyzer(Transactions)

        setup(back_trader)

        start_value = back_trader.broker.getvalue()
        print('Starting Portfolio Value: %.2f' % start_value)

        back_trader.addstrategy(MorningBuy, silent=silent_strategy, open_hour=2, close_hour=13)
        strategies = back_trader.run(timeframe=bt.TimeFrame.Minutes)

        print('\nAnalysis:')
        for strategy in strategies:
            separator_length = 0
            print('==================================')
            print('Strategy:', strategy.__class__.__name__)
            if isinstance(strategy, Base):
                strategy_info = strategy.strategy_info()
                if strategy_info:
                    print("Description: %s" % strategy_info)

            strategy.analyzers.annualreturn.print(seplen=separator_length)
            strategy.analyzers.sharpe.print(seplen=separator_length)
            strategy.analyzers.sqn.print(seplen=separator_length)
            OrdersTrades.print_orders(list(strategy.analyzers.transactions.rets.items()))
        print('==================================')

        end_value = back_trader.broker.getvalue()
        percents = (end_value - start_value) / start_value * 100
        print('Final portfolio value: %.2f (%.2f percents)' % (back_trader.broker.getvalue(), percents))

        titles = ["%s - %s" % (ticker, strategy.__class__.__name__) for strategy in strategies]
        plotter = Plotter(titles)

        back_trader.plot(plotter=plotter)
    else:
        opt_results = []

        back_trader = bt.Cerebro()
        back_trader.addanalyzer(TimeReturn, timeframe=bt.TimeFrame.Years, _name='annualreturn')
        back_trader.addanalyzer(Transactions)

        open_hour = range(1, 12)
        close_hour = range(12, 18)

        back_trader.optstrategy(MorningBuy, open_hour=open_hour, close_hour=close_hour, silent=True)
        back_trader.optcallback(optimization_callback)

        setup(back_trader)
        strategies = back_trader.run()

        def set_percent(x):
            x0 = x[0] * 100
            return x0, x[1], x[2]
        opt_results = [set_percent(x) for x in opt_results]

        def print_result(x):
            print(tabulate(x, headers=["Profit", "Open hour", "Close hour"], floatfmt=".2f", tablefmt="simple"))

        print()
        print("%s %s - %s" % (ticker, Date.date_str(from_date), Date.date_str(to_date)))
        print("Open hours: %s - %s" % (open_hour.start, open_hour.stop))
        print("Close hours: %s - %s" % (close_hour.start, close_hour.stop))
        # print("\nOptimization\n")
        # print_result(opt_results)
        opt_results.sort(key=lambda x: x[0], reverse=True)
        print("\nSorted by profit\n")
        print_result(opt_results)
