#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import datetime
import backtrader as bt
from backtrader.sizers import AllInSizer

from main.lab.data.remote import Remote
from main.lab.strategy import TradeMode
from main.lab.strategy.ml import BaseML
from main.lab.tools.plotter import Plotter
from main.tool.date import Date


def plot(ticker, back_trader, strategies, test_date=0):
    titles = []
    for strategy in strategies:
        title = "%s - %s" % (ticker, strategy.__class__.__name__)
        if test_date != 0:
            title += " (%s)" % Date.date_str(test_date)
        titles.append(title)
    plotter = Plotter(titles)
    back_trader.plot(plotter=plotter, start=test_date)


def get_pass_date(days):
    pass_date = datetime.datetime.today() - datetime.timedelta(days=days)
    pass_date = pass_date.replace(hour=0, minute=0, second=0, microsecond=0)

    return pass_date


def find_signals(config, company, strategies_to_add, from_date, to_date, pass_date, test_date=None):

    back_trader = bt.Cerebro()

    for strategy_to_add in strategies_to_add:
        if issubclass(strategy_to_add, BaseML):
            back_trader.addstrategy(strategy_to_add, trade_mode=TradeMode.Pass, trade_pass_date=pass_date, silent=True,
                                    test_date=test_date, silent_ml=True)
        else:
            back_trader.addstrategy(strategy_to_add, trade_mode=TradeMode.Pass, trade_pass_date=pass_date, silent=True)

    data = Remote.get_data(config, company, from_date, to_date)
    back_trader.adddata(data)

    back_trader.broker.setcash(1000.0)
    back_trader.addsizer(AllInSizer)
    back_trader.broker.setcommission(commission=0.0)

    strategies = back_trader.run()

    strategy_names = []
    signals = []
    for strategy in strategies:
        orders = strategy._orders
        # if len(orders) % 2 == 1:
        strategy_names.append(strategy.__class__.__name__)
        signals.append(orders)

    # plot(ticker, back_trader, strategies, test_date)

    return strategy_names, signals
