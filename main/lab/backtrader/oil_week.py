#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
from backtrader.analyzers import (SQN, AnnualReturn, SharpeRatio_A, Transactions)

import main.lab.strategy.ml.regression.inventory as inv_str
from main.config.config import Config
from main.lab.indicator.inventory import PlotDataStyle
from main.lab.report.orders_trades import OrdersTrades
from main.lab.sizer.percents_sizer import AllInSizer
from main.lab.strategy.calendar.morning_buy import Base
# from main.lab.strategy.indicator import RSISimple
from main.lab.tools.plotter import Plotter
from main.source.investing.inventory.source import *
from main.source.quandl.quandl import Quandl
from main.tool.date import Date
from main.tool.resource_locator import ResourceLocator, ResourceType

# ticker = 'BRENT'
ticker = 'WTI'
from_date, to_date = Date.last_months(4)
offline_mode = 0

config_path = ResourceLocator.get_resource(ResourceType.ConfigFile)
config = Config(config_path)
back_trader = bt.Cerebro()
silent_strategy = True

# Oil data
quandl_section = config.config_section_map('SourceQuandl')
quandl = Quandl(quandl_section['api_key'], "CHRIS/CME_CL1")
oil_price = quandl.get_history(from_date, to_date)

oil_price_data = bt.feeds.PandasData(dataname=oil_price)
back_trader.adddata(oil_price_data)

# Contains
# 0. Source
# 1. Group
# 2. Plot data style
# 3. Plot method
# 4. Data field ("actual" otherwise)
sources = [
    (API(), 0, PlotDataStyle.Peak, None, None),
    (Crude(), 0, PlotDataStyle.Peak, None, None),
    (Distillates(), 0, PlotDataStyle.Peak, None, None),
    (Gasoline(), 0, PlotDataStyle.Peak, None, None),
    (Imports(), 0, PlotDataStyle.Peak, None, None),
    (RigsCount(), 1, PlotDataStyle.Peak, "bar", "change"),
]

# Setup
back_trader.addanalyzer(AnnualReturn)
back_trader.addanalyzer(SharpeRatio_A, timeframe=bt.TimeFrame.Days, _name='sharpe')
back_trader.addanalyzer(SQN, _name='sqn')
back_trader.addanalyzer(Transactions)

back_trader.broker.setcash(1000.0)
back_trader.addsizer(AllInSizer)
back_trader.broker.setcommission(commission=0.0)

start_value = back_trader.broker.getvalue()
print('Starting Portfolio Value: %.2f' % start_value)

test_percents = 80
test_date = Date.dates_delta(from_date, to_date, test_percents)
# back_trader.addstrategy(RSISimple, silent=silent_strategy)
back_trader.addstrategy(inv_str.Inventory, silent=silent_strategy, offline_mode=offline_mode,
                        sources=sources, from_date=from_date, test_date=test_date, silent_ml=True)
strategies = back_trader.run()

print('\nAnalysis:')
for strategy in strategies:
    separator_length = 0
    print('==================================')
    print('Strategy:', strategy.__class__.__name__)
    if isinstance(strategy, Base):
        strategy_info = strategy.strategy_info()
        if strategy_info:
            print("Description: %s" % strategy_info)

    strategy.analyzers.annualreturn.print(seplen=separator_length)
    strategy.analyzers.sharpe.print(seplen=separator_length)
    strategy.analyzers.sqn.print(seplen=separator_length)
    OrdersTrades.print_orders(list(strategy.analyzers.transactions.rets.items()))
print('==================================')

end_value = back_trader.broker.getvalue()
percents = (end_value - start_value) / start_value * 100
print('Final portfolio value: %.2f (%.2f percents)' % (back_trader.broker.getvalue(), percents))

titles = ["%s - %s" % (ticker, strategy.__class__.__name__) for strategy in strategies]
plotter = Plotter(titles)
back_trader.plot(plotter=plotter)
