#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
from backtrader import num2date

from main.source.investing.earning.earning_dates import EarningDates


class EarningIndicator(bt.Indicator):

    params = (
        ("offline_mode", 0),
        ("earning", None),  # Share earning meta
        ("skip_before", 1),
        ("skip_after", 2),
        ("cache_days", 30),
    )

    lines = ("earning_period",)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.skip_days = []

        a = EarningDates(self.params.offline_mode, self.params.earning,
                         self.params.cache_days, self.params.skip_before, self.params.skip_after)

        self.skip_days = a.get_skip_dates()

    def next(self):
        current_date = num2date(self.data.datetime[0]).date()
        if current_date in self.skip_days:
            self.lines.earning_period[0] = 1.0
        else:
            self.lines.earning_period[0] = 0
