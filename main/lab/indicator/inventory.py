#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
from main.source.investing.inventory import Loader


class PlotDataStyle:
    Peak, Continues = range(2)


class Inventory(bt.Indicator):

    params = (
        ("offline_mode", 0),
        ("sources", None),
        ("plot_data_type", PlotDataStyle.Peak),
        ("from_date", None)
    )

    lines = ("data0", "data1", "data2", "data3", "data4", "data5", "data6", "data7", "data8", "data9")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.sources = self.params.sources
        self.sources_data = []

        # Check parameters
        sources_count = len(self.sources)
        lines_count = self.lines.size()
        if sources_count > lines_count:
            raise ValueError('Data sources count (%d) is larger than available lines count (%d)'
                             % (sources_count, lines_count))

        # Load data
        loader = Loader()
        for index, source in enumerate(self.params.sources):
            data = loader.get_data(source[0], 0, self.params.from_date)
            self.sources_data.append(data)

        # Setup lines
        for index in range(0, lines_count):
            plot_line = getattr(self.plotlines, 'data' + str(index))
            if index < sources_count:
                # Name line
                source = self.sources[index][0]
                plot_line._name = source.name
                plot_line._plotvalue = False
                name = self.sources[index][4]
                if name:
                    plot_line._method = self.sources[index][3]
            else:
                # Hide line
                plot_line._plotskip = True

    @staticmethod
    def find_data(sources_data, date):
        result = None

        if not sources_data:
            return None

        for data in sources_data:
            if data.date.date() == date:
                result = data
                break

        return result

    def _plotlabel(self):
        return []  # Do not print indicator parameters. What for? Sources name will be in lines legend

    def _plotinit(self):
        pass

    def next(self):
        current_date = bt.num2date(self.data.datetime[0]).date()

        source_count = len(self.sources)
        for index in range(0, source_count):
            source_data = self.sources_data[index]
            data = self.find_data(source_data, current_date)
            if data:
                field_name = self.sources[index][4]
                if field_name:
                    field = getattr(data, field_name)
                    self.lines[index][0] = field
                else:
                    self.lines[index][0] = data.actual
                    pass
            else:
                if self.sources[index][2] == PlotDataStyle.Peak:
                    self.lines[index][0] = 0
                else:
                    self.lines[index][0] = self.lines[index][-1]
