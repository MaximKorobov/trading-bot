#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


class Data:
    @staticmethod
    def indicator_list(indicator_list, levels):
        correct = True
        for index, level in enumerate(levels):
            rsi = indicator_list[-index]
            if level > 0:
                condition = (rsi > abs(level))
            else:
                condition = (rsi < abs(level))
            correct = correct and condition
        return correct

    @staticmethod
    def i_l(indicator_list, levels):
        return Data.indicator_list(indicator_list, levels)

    @staticmethod
    def position_profit(price, position):
        is_buy = position.size > 0
        if is_buy:
            result = (price - position.price) / price * 100
        else:
            result = (position.price - price) / position.price * 100

        return result
