#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt


class WarmupDetector:

    @staticmethod
    def detect(strategies):
        greatest_warm_up_period, strategy_name = 0, ""

        runner = bt.Cerebro()
        for sta in strategies:
            runner.addstrategy(sta, silent=True)
        stub_data = bt.DataBase()
        runner.adddata(stub_data)

        sis = runner.run(runonce=False)

        for si in sis:
            period = si.get_warm_up_period()
            if period > greatest_warm_up_period:
                greatest_warm_up_period, strategy_name = period, si.__class__.__name__

        return greatest_warm_up_period, strategy_name

    @staticmethod
    def detect_period(strategies):
        return WarmupDetector.detect(strategies)[0]
