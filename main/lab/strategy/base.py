#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import abstractmethod, ABCMeta

import backtrader as bt
from backtrader.utils import num2date

from main.lab.indicator.earning import EarningIndicator
from main.lab.tools.data import Data


class BaseMeta(ABCMeta, type(bt.Strategy)):
    """
    That's why this class should be:
        - http://stackoverflow.com/questions/41659630/
        - https://community.backtrader.com/topic/114/strategy-inheritance/6
    """
    pass


class TradeMode:
    """

    Trade modes of strategy:
        - Skip (default) - ignore loaded trades
        - Pass - ignore trades until most recent not closed
        - Play - make all trades once again

    """
    Skip, Pass, Play = range(3)


class SkipReason:
    """

    Reasons of skip next() call in derived strategy:
        - OrderExists - order already exists. Only one order per strategy supp
        - NewOrder - new order was found for current day. Should execute it instead of strategy action
        - PassDate - new orders will be later. Wait for them 
        - Earnings - current day is in earning report days

    """
    OrderExists, NewOrder, PassDate, Earnings = range(4)


class Base(bt.Strategy, metaclass=BaseMeta):

    params = (
        ('offline_mode', 0),
        ('silent', False),
        ('trade_mode', TradeMode.Skip),
        ('trade_pass_date', None),
        ('orders_storage', None),
        ('earning', None),
        ('earning_skip_before', 0),
        ('earning_skip_after', 0)
    )

    @abstractmethod
    def strategy_info(self):
        """Strategy text information"""
        return ""

    @abstractmethod
    def strategy_urls(self):
        """URLs to describe strategy's logic"""
        return list()

    def get_warm_up_period(self):
        """Return list of tuple where tuple_1 is indicator name, tuple_2 is warmup period in days"""
        return self.warm_up_period

    def log(self, txt, dt=None):
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def log_order(self, order):
        data = ''
        if order.isbuy():
            data += "BUY   "
        else:
            data += "SELL  "

        data += ' | Price: %.2f | Cost: %.2f ' % (order.executed.price, order.executed.value)
        if not isinstance(self.sizer, bt.sizers.FixedSize):
            data += "| Size %.2f" % order.executed.size

        self.log(data)

    # ------------------------------------------------------------------------------------------------
    # Time series simplification
    # ------------------------------------------------------------------------------------------------
    def day_change(self, index=0):
        return self.datas[0].close[index] - self.datas[0].close[index-1]

    def day_change_percents(self, index=0):
        d_c = self.datas[0].close[index] - self.datas[0].close[index - 1]

        d_c_p = d_c / self.datas[0].close[index - 1] * 100

        return d_c_p

    def get_previous_days(self, index, count=10):
        result = []
        for i in range(0, count):
            price = self.data_close[index - i]
            result.append(price)
        return result

    # ------------------------------------------------------------------------------------------------

    def __init__(self):
        super().__init__()

        self.data_close = self.datas[0].close

        self.order = None
        self.bar_executed = 0

        self.buys_total = 0
        self.buys_profit = 0
        self.sells_total = 0
        self.sells_profit = 0

        # Some basic tools
        self.two_bars = 0
        self.profit_percents = 0
        self.today_date = 0

        self.warm_up_period = 0

        if self.params.trade_mode != TradeMode.Skip:
            self.storage = self.params.orders_storage
            self.trade_pass_date = self.params.trade_pass_date

        self.earnings = None
        if self.params.earning:
            self.earnings = EarningIndicator(earning=self.params.earning,
                                             skip_before=self.params.earning_skip_before,
                                             skip_after=self.params.earning_skip_after)

    def start(self):
        self.warm_up_period = self._minperiod

    def notify_order(self, order):

        if order.status in [order.Submitted, order.Accepted]:
            return  # Buy/Sell order submitted/accepted to/by broker - Nothing to do

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed, order.Canceled, order.Margin]:
            if order.executed.value == 0:
                if not self.params.silent:
                    self.log('ORDER SKIPPED - no funds\n')
            else:
                if not self.params.silent:
                    self.log_order(order)

                if order.isbuy():
                    if self.position.upclosed > 0:
                        self.sells_total += 1
                else:
                    if self.position.upclosed < 0:
                        self.buys_total += 1

                self.bar_executed = len(self)

        self.order = None  # Write down: no pending order

    def execute_order(self, order):

        if order.lot > 0:
            self.buy(size=order.lot)
        else:
            self.sell(size=order.lot)

        if not self.params.silent:
            print("Execute order: %s" % order)

    def next(self):

        self.two_bars = len(self) >= (self.bar_executed + 2)
        if self.position:
            self.profit_percents = Data.position_profit(self.data_close[0], self.position)
        else:
            self.profit_percents = 0
        self.today_date = num2date(self.datas[0].datetime[0])

        if self.order:
            return SkipReason.OrderExists

        if self.params.earning and self.earnings[0] > 0:
            return SkipReason.Earnings

        if self.params.trade_mode != TradeMode.Skip:
            if self.storage:
                most_recent_trade = self.storage.get_most_recent_order()
                if most_recent_trade is not None:
                    if self.params.trade_mode == TradeMode.Pass:
                        if most_recent_trade > self.today_date:
                            return SkipReason.PassDate
                        else:
                            return  # Start strategy trading
                    elif self.params.trade_mode == TradeMode.Play:
                        order = self.storage.get_order(self.today_date)
                        if order:
                            self.execute_order(order)
                            return SkipReason.NewOrder
                        else:
                            if most_recent_trade > self.today_date:
                                return SkipReason.PassDate
                            else:
                                return  # Start strategy trading
            elif self.trade_pass_date:
                if self.trade_pass_date > self.today_date:
                    return SkipReason.PassDate
            else:
                pass

    def notify_trade(self, trade):
        if not trade.isclosed:
            return

        if not self.params.silent:
            trade_percents = trade.pnlcomm / self.broker.getcash() * 100
            self.log('PROFIT | Gross: %.2f | Net %.2f | Percents %.2f\n' %
                     (trade.pnl, trade.pnlcomm, trade_percents))

    def stop(self):
        if not self.params.silent:
            print("Buys total %.0f, profit percents %.2f" % (self.buys_total, self.buys_profit))
            print("Sells total %.0f, profit percents %.2f" % (self.sells_total, self.sells_profit))
            self.log('Ending value %.2f' % (self.broker.getvalue()))
