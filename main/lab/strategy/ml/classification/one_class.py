#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
import numpy as np
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from main.lab.strategy.ml import BaseML


class OneClass(BaseML):

    params = (
        ('knn', 11),
    )

    def strategy_info(self):
        return "Strategy uses machine learning classifiers to answer simple buy or sell today?"

    def strategy_urls(self):
        return ["https://www.quantopian.com/posts/simple-machine-learning-example", "https://habrahabr.ru/post/215453/",
                "http://www.financial-hacker.com/build-better-strategies-part-5-developing-a-machine-learning-system/"]

    def get_method_category(self):
        return BaseML.MethodCategory.Classification

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.MovingAverageSimple(period=15)
        self.rsi = bt.indicators.RSI_Safe(RSI_Safe=3, upperband=70, lowerband=30)
        self.b_bands = bt.ind.BollingerBands()
        self.ema = bt.ind.ExponentialMovingAverage()
        self.adx = bt.ind.PlusDirectionalIndicator()

        self.classifier = None

    def get_feature(self, index):
        # Default
        return [self.rsi[index], self.sma[index]]

        # All
        # return [
        #     self.rsi[index], self.sma[index], self.ema[index],
        #     self.adx[index],
        #     self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #     self.data_close[index-1], self.data_close[index-2], self.data_close[index-3],
        #     self.datas[0].volume[index]
        #         ]

    def next(self):
        if super().next():
            return

        if len(self.data_close) > 30:
            if not self.classifier:
                self.x_train.append(self.get_feature(-1))
                self.y_train.append(self.day_change(0) > 0)

            if self.today > self.params.test_date:
                pass

                if not self.classifier:
                    knn = self.params.knn
                    self.classifier = KNeighborsClassifier(n_neighbors=knn)
                    # self.classifier = AdaBoostClassifier(random_state=0)
                    # self.classifier = RandomForestClassifier(random_state=0)
                    # self.classifier = MLPClassifier(random_state=0)
                    # self.classifier = SVC(gamma=2, C=1, random_state=0, probability=True)
                    # self.classifier = GradientBoostingClassifier(random_state=0)

                    # self.x = sklearn.preprocessing.scale(self.x)
                    self.classifier.fit(self.x_train, self.y_train)

                    # Features importances
                    if not self.params.silent_ml:
                        try:
                            features_importance = getattr(self.classifier, "feature_importances_")
                            if features_importance is not None:
                                # importance = self.classifier.feature_importances_
                                print("Features importance:")
                                print(features_importance)
                        except AttributeError:
                            print("Features importance is not available")

                today_change = self.get_feature(0)
                x0 = np.asarray(today_change).reshape(1, -1)
                prediction = self.classifier.predict(x0)
                probability1, probability2 = self.classifier.predict_proba(x0)[0]

                self.stats_data.append((self.today, 1 if self.day_change(0) > 0 else 0, prediction[0]))

                if not self.position:
                    threshold = 0.65
                    if probability1 >= threshold:
                        self.buy()
                    elif probability2 >= threshold:
                        self.sell()
                else:
                    if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                        self.order = self.close()
