#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
from backtrader.utils import num2date
import numpy as np
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from main.lab.strategy.ml import BaseML


class VotingClass(BaseML):

    def strategy_info(self):
        return "Strategy uses all popular classifiers, then uses voting classifier to decide buy or sell?"

    def strategy_urls(self):
        return ["http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.VotingClassifier.html"]

    def get_method_category(self):
        return BaseML.MethodCategory.Classification

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.MovingAverageSimple(period=15)
        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)
        self.b_bands = bt.ind.BollingerBands()
        self.ema = bt.ind.ExponentialMovingAverage()
        self.adx = bt.ind.PlusDirectionalIndicator()

        self.classifier = None
        self.classifiers = []

        self.classifiers1 = [
            KNeighborsClassifier(3),
            SVC(kernel="linear", C=0.025, random_state=0, probability=True),
            SVC(gamma=2, C=1, random_state=0, probability=True),
            GaussianProcessClassifier(1.0 * RBF(1.0), warm_start=True, random_state=0),
            DecisionTreeClassifier(max_depth=5, random_state=0),
            RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1, random_state=0),
            # MLPClassifier(alpha=1, random_state=0),
            # AdaBoostClassifier(random_state=0),
            # GaussianNB(),
            # QuadraticDiscriminantAnalysis()
        ]

    def get_feature(self, index):
        return [self.rsi[index], self.sma[index]]
        # return [self.rsi[index], self.sma[index], self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #         self.datas[0].volume[index]]
        # return [self.rsi[index], self.sma[index], self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #         self.ema[index], self.adx[index]]
        # return [self.rsi[index], self.sma[index], self.ema[index], self.adx[index],
        #         self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #         self.data_close[index-1], self.data_close[index-2], self.data_close[index-3],
        #         self.datas[0].volume[index]]

    def next(self):
        if super().next():
            return

        if len(self.data_close) > 30:
            self.x_train.append(self.get_feature(-1))
            self.y_train.append(self.day_change(0) > 0)

            if self.today > self.params.test_date:
                pass

                if not self.classifier:
                    for clf in self.classifiers1:
                        clf.fit(self.x_train, self.y_train)
                        self.classifiers.append((clf.__class__.__name__, clf))

                    self.classifier = VotingClassifier(estimators=self.classifiers, voting='soft')
                    self.classifier.fit(self.x_train, self.y_train)

                today_change = self.get_feature(0)
                x0 = np.asarray(today_change).reshape(1, -1)
                prediction = self.classifier.predict(x0)[0]
                probability1, probability2 = self.classifier.predict_proba(x0)[0]

                self.stats_data.append((self.today, 1 if self.day_change(0) > 0 else 0, prediction))

                if not self.position:
                    threshold = 0.65
                    if probability1 >= threshold:
                        self.buy()
                    elif probability2 >= threshold:
                        self.sell()
                else:
                    if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                        self.order = self.close()
