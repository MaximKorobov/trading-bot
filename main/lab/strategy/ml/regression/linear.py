#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import normalize

from main.lab.strategy.ml import BaseML


class Linear(BaseML):
    def strategy_info(self):
        return "Strategy uses machine learning linear regression to consider simple buy or sell today"

    def strategy_urls(self):
        return ["http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html"]

    def get_method_category(self):
        return BaseML.MethodCategory.Regression

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.MovingAverageSimple(period=15)
        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)
        self.ema = bt.ind.ExponentialMovingAverage()
        self.adx = bt.ind.PlusDirectionalIndicator()
        self.b_bands = bt.ind.BollingerBands()

        self.regressor = None

    def get_feature(self, index):
        # Default
        # return [self.rsi[index], self.sma[index]]

        # All
        return [
            self.rsi[index], self.sma[index], self.ema[index],
            self.adx[index],
            self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
            self.data_close[index-1], self.data_close[index-2], self.data_close[index-3],
            self.datas[0].volume[index]
                ]

    def next(self):
        if super().next():
            return

        if len(self.data_close) > 40:
            day_change = self.day_change(-1)
            self.x_train.append(self.get_feature(-2))
            self.y_train.append(day_change)

            if self.today > self.params.test_date:
                pass

                if not self.regressor:
                    self.regressor = LinearRegression()
                    x_norm = normalize(self.x_train)
                    self.regressor.fit(x_norm, self.y_train)
                    # self.regressor.fit(self.x, self.y)

                today_change = self.get_feature(-1)
                x0 = np.asarray(today_change).reshape(1, -1)

                prediction = self.regressor.predict(X=x0)[0]

                self.stats_data.append((self.today, self.day_change(-1), prediction))

                if not self.position:
                    prediction_percents = prediction / self.data_open[0] * 100
                    percents_threshold = 0.65
                    if prediction_percents > percents_threshold:
                        self.buy()
                    elif prediction_percents < -percents_threshold:
                        self.sell()
                        # if len(self.stats_data) > 2:
                        #     prev_prediction = self.stats_data[-2][2]
                        #
                        #     if prediction > 0 and prev_prediction > 0:
                        #         self.buy()
                        #     elif prediction < 0 and prev_prediction < 0:
                        #         self.sell()
                else:
                    if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                        self.order = self.close()
