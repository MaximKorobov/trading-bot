#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from itertools import groupby

import tabulate
import math
import numpy
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler

import main.lab.indicator.inventory as inv_id
import backtrader as bt

from main.lab.strategy.ml import BaseML
from main.tool.date import Date


class Inventory(BaseML):

    params = (
        ("offline_mode", 0),
        ("sources", None),
        ("one_indicator_per_list", True),
        ("from_date", None)
    )

    def strategy_info(self):
        return "Strategy uses oil inventory to predict price after time frame"

    def strategy_urls(self):
        return []

    def get_method_category(self):
        return BaseML.MethodCategory.Regression

    def __init__(self):
        super().__init__()

        sources = getattr(self.params, 'sources')
        self.grouped_sources = [list(g) for k, g in groupby(sources, lambda s: s[1])]

        self.inv_indicators = []
        for sources in self.grouped_sources:
            ind = inv_id.Inventory(sources=sources, from_date=self.params.from_date, test_date=self.params.test_date)
            self.inv_indicators.append(ind)

        self.sma = bt.indicators.MovingAverageSimple()
        self.sma.params.period = 10

        self.regressor = None
        self.scaler = StandardScaler()

    def get_feature(self, index):
        features = []

        # sma = self.sma[index]
        # features.append(0 if math.isnan(sma) else sma)

        for source_index, sources in enumerate(self.grouped_sources):
            indicator = self.inv_indicators[source_index]
            for index, source in enumerate(sources):
                line = indicator.lines[index][0]
                features.append(line)

        return features

    def next(self):
        if super().next():
            return

        if len(self.data_open) > self.sma.params.period:
            feature = self.get_feature(-2)
            self.x_train.append(feature)
            self.y_train.append(self.data_open[-1])

            if self.today > self.params.test_date:
                if not self.regressor:
                    self.regressor = MLPRegressor(random_state=0, max_iter=2000, verbose=False)

                    self.x_scaled = self.x_train# self.scaler.fit_transform(self.x_train)
                    # self.x_scaled = self.scaler.fit_transform(self.x_train)

                    self.regressor.fit(self.x_scaled, self.y_train)

                feature = self.get_feature(-1)
                feature = numpy.asarray(feature).reshape(1, -1)

                prediction = self.regressor.predict(X=feature)[0]
                prediction_percents = (prediction / self.data_open[0] - 1) * 100

                self.stats_data.append((Date.date_str(self.today), self.y_train[-1], prediction, prediction_percents))

                percents_threshold = 2
                if not self.position:
                    if prediction_percents > percents_threshold:
                        self.order = self.buy()
                    elif prediction_percents < -percents_threshold:
                        self.order = self.sell()
                else:
                    # General TP/SL
                    if 3 < self.profit_percents or self.profit_percents < -1:
                        self.close()
                    else:
                        if self.position.size > 0:
                            if prediction_percents > percents_threshold:
                                pass  # Hold
                            else:
                                self.close()  # Close negative profit orders
                        else:
                            if prediction_percents < -percents_threshold:
                                pass  # Hold
                            else:
                                self.close()  # Close negative profit orders

    def stop(self):
        super().stop()

        # print(tabulate.tabulate(self.x_train))
