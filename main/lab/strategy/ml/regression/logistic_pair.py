#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.neural_network import MLPRegressor

from main.lab.strategy.ml import BaseML
from main.tool.date import Date


class LogisticPair(BaseML):
    def strategy_info(self):
        return "First predict price jump in X %; Second predict price drop in Y %. " \
               "If they're both confident in their predictions (according to some threshold), place an order."

    def strategy_urls(self):
        return ["https://medium.com/@TalPerry/deep-learning-the-stock-market-df853d139e02"]

    def get_method_category(self):
        return BaseML.MethodCategory.Regression

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.MovingAverageSimple(period=15)
        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)

        self.regressor1 = None
        self.regressor2 = None

        self.y_train1 = []
        self.y_train2 = []

    def get_feature(self, index):
        volume = self.datas[0].volume[index] / 100000
        return [self.rsi[index], self.sma[index], self.day_change(index), volume]

    def next(self):
        if super().next():
            return

        if len(self.data_close) > 40:
            day_change = self.day_change(-1)
            day_change_p = self.day_change_percents(-1)
            self.x_train.append(self.get_feature(-2))

            self.y_train1.append(day_change_p > 0)
            self.y_train2.append(day_change_p < -1)

            if self.today > self.params.test_date:
                pass

                if not self.regressor1:
                    # x_norm = normalize(self.x_train)

                    self.regressor1 = LogisticRegression(random_state=0)
                    self.regressor1.fit(self.x_train, self.y_train1)

                    self.regressor2 = LogisticRegression(random_state=0)
                    self.regressor2.fit(self.x_train, self.y_train2)

                today_change = self.get_feature(-1)
                x0 = np.asarray(today_change).reshape(1, -1)

                prediction1 = self.regressor1.predict(x0)[0]
                prediction1_proba = self.regressor1.predict_proba(x0)[0][0]

                if prediction1:
                    self.stats_data.append((self.today, prediction1, prediction1_proba))

                prediction2 = self.regressor2.predict(x0)[0]
                prediction2_proba = self.regressor2.predict_proba(x0)[0][0]
                if prediction2:
                    self.stats_data.append((self.today, prediction2, prediction2_proba))

                if not self.position:

                    # if prediction1 > 1:
                    #     self.buy()
                    #
                    if prediction1 and prediction1_proba > 0.39:
                        if prediction2 and prediction2_proba > 0.39:
                            print("SKIP IT on " + Date.date_str(self.today))
                        else:
                            self.buy()
                else:
                    if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                        self.order = self.close()
