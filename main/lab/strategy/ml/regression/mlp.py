#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt
import numpy as np
from sklearn.neural_network import MLPRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import normalize

from main.lab.strategy.ml import BaseML
from main.tool.date import Date


class NormType:
    Normalize, Scale, DoNothing = range(3)


class MLP(BaseML):

    params = (
        ("norm_type", NormType.Scale),
    )

    def strategy_info(self):
        return "MLP (Multi-Layer Perceptron) to consider simple buy or sell today and keep holding if model suggest it"

    def strategy_urls(self):
        return ["http://scikit-learn.org/stable/modules/neural_networks_supervised.html#regression"]

    def get_method_category(self):
        return BaseML.MethodCategory.Regression

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.MovingAverageSimple(period=15)
        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)
        self.ema = bt.ind.ExponentialMovingAverage()
        self.adx = bt.ind.PlusDirectionalIndicator()
        self.b_bands = bt.ind.BollingerBands()

        self.regressor = None
        self.scaler = StandardScaler()

    def get_feature(self, index):
        return [self.rsi[index], self.sma[index], self.day_change(index),
                self.day_change(index - 1), self.adx[index]]
        # return [self.rsi[index - 1], self.sma[index - 1]] + self.get_previous_days(index, 5)
        # return [self.data_close[index - 2], self.data_close[index - 5],
        # self.data_close[index - 10], self.data_close[index - 20]]
        # return [self.rsi[index], self.sma[index], self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #         self.datas[0].volume[index]]
        # return [self.rsi[index], self.sma[index], self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
        #         self.ema[index], self.adx[index]]
        # return [self.rsi[index], self.sma[index], self.ema[index], self.adx[index],
                # self.b_bands.lines.top[index], self.b_bands.lines.bot[index],
                # self.data_close[index-1], self.data_close[index-2], self.data_close[index-3],
                # self.datas[0].volume[index]
                # ]

    def scale_x(self, norm_type, x, y=None):
        if norm_type == NormType.Normalize:
            return normalize(x)  # Good for NFLX data (but non-presentative values in logs)
        elif norm_type == NormType.Scale:
            return self.scaler.fit_transform(x, y)  # Good for FB Data
        elif norm_type == NormType.DoNothing:
            return x

    def next(self):
        if super().next():
            return

        if len(self.data_open) > 30:
            self.x_train.append(self.get_feature(-2))
            self.y_train.append(self.data_close[-1])

            if self.today > self.params.test_date:
                norm_type = self.params.norm_type

                # Train once
                if not self.regressor:
                    self.regressor = MLPRegressor(random_state=0, max_iter=2000, verbose=False)

                    self.x_scaled = self.scale_x(norm_type, self.x_train, self.y_train)

                    self.regressor.fit(self.x_scaled, self.y_train)

                # Prepare new feature
                today_change = self.get_feature(-1)
                x0 = np.asarray(today_change).reshape(1, -1)
                if norm_type == NormType.Normalize:
                    pass  # TODO: normalize
                    # prediction_scaled = self.scaler.inverse_transform(prediction)
                elif norm_type == NormType.Scale:
                    x0 = self.scaler.transform(x0)
                elif norm_type == NormType.DoNothing:
                    pass

                prediction = self.regressor.predict(X=x0)[0]
                prediction_percents = (prediction / self.data_open[0] - 1) * 100

                self.stats_data.append((Date.date_str(self.today), self.y_train[-1], prediction, prediction_percents))

                percents_threshold = 2
                if not self.position:
                    if prediction_percents > percents_threshold:
                        self.order = self.buy()
                    elif prediction_percents < -percents_threshold:
                        self.order = self.sell()
                else:
                    # General TP/SL
                    if 3 < self.profit_percents or self.profit_percents < -1:
                        self.close()
                    else:
                        if self.position.size > 0:
                            if prediction_percents > percents_threshold:
                                pass  # Hold
                            else:
                                self.close()  # Close negative profit orders
                        else:
                            if prediction_percents < -percents_threshold:
                                pass  # Hold
                            else:
                                self.close()  # Close negative profit orders
