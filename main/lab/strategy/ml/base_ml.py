#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from abc import ABCMeta, abstractmethod

import numpy
import backtrader as bt
from sklearn.metrics import *
from tabulate import tabulate

from main.lab.strategy import Base


class BaseML(Base, metaclass=ABCMeta):

    class MethodCategory:
        Classification, Regression = range(2)

    params = (
        ("test_date", None),
        ("silent_ml", False),
    )

    @abstractmethod
    def get_method_category(self):
        return None

    def __init__(self):
        super().__init__()

        self.x_train = []
        self.y_train = []
        self.x_scaled = []
        self.y_scaled = []

        self.stats_data = []
        self.today = None

        if self.params.test_date is None:
            print("Warning! Please provider test date parameter for machine learning-based strategy!")
            raise bt.StrategySkipError

    def next(self):
        super_result = super().next()
        if super_result:
            return super_result

        self.today = bt.num2date(self.datas[0].datetime[0])

    def stop(self):
        pass
        if not self.params.silent_ml:
            print()
            print("Predictions:")
            print(tabulate(self.stats_data, headers=["Date", "Data (Y)", "Prediction", "%"], floatfmt=".2f"))
            print()

            # Score: http://scikit-learn.org/stable/modules/model_evaluation.html
            y = [y[1] for y in self.stats_data]
            y_predicted = [y[2] for y in self.stats_data]
            if len(y) == 0:
                return

            print("Correlations (LINEAR) between features and target:")
            for x_index in range(0, len(self.x_train[0])):
                xn = [i[x_index] for i in self.x_train]
                if len(self.y_train) > 0:
                    corr_matrix = numpy.corrcoef(xn, self.y_train)
                    corr = corr_matrix[1, 0]
                    print("%s: %.2f" % (x_index, corr))
            print()

            if self.get_method_category() == BaseML.MethodCategory.Classification:
                accuracy = accuracy_score(y, y_predicted)
                precision = precision_score(y, y_predicted)
                recall = recall_score(y, y_predicted)
                f1 = f1_score(y, y_predicted)
                print("Scores:")
                print(tabulate([[accuracy, precision, recall, f1]], ["Accuracy", "Precision", "Recall", "F1 Score"]))
                print()

                cm = confusion_matrix(y, y_predicted, labels=[1, 0])
                print("Confusion matrix:")
                print(tabulate(cm))
                print()

            elif self.get_method_category() == BaseML.MethodCategory.Regression:
                mae = mean_absolute_error(y, y_predicted)
                mse = mean_squared_error(y, y_predicted)
                print(tabulate([[mae, mse]], ["Mean absolute error", "Mean square error"]))
            print()
