#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base
from main.lab.tools.data import Data


class OpenClose(Base):

    def strategy_urls(self):
        return [""]

    def strategy_info(self):
        return "Buy when open is lower than previous day close"

    def __init__(self):
        super().__init__()

        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)

    def next(self):
        if super().next():
            return

        if not self.position:
            if self.datas[0].open[0] < self.datas[0].close[-1] and Data.indicator_list(self.rsi, [70, -30]):
                self.order = self.buy()
            elif self.datas[0].open[0] > self.datas[0].close[-1] and Data.indicator_list(self.rsi, [-30, 70]):
                self.order = self.sell()
        else:
            take_percents = self.profit_percents > 3
            loose_percents = self.profit_percents < -1

            if take_percents or loose_percents:
                self.order = self.close()
