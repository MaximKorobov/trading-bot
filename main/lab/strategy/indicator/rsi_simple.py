#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base
from main.lab.tools.data import Data


class RSISimple(Base):

    def strategy_urls(self):
        return ["https://github.com/mementum/backtrader/blob/master/samples/kselrsi/ksignal.py"]

    def strategy_info(self):
        return "Human-made strategy based on history observations"

    def __init__(self):
        super().__init__()

        self.sma = bt.indicators.SimpleMovingAverage(self.datas[0], period=15)
        self.rsi = bt.indicators.RSI_Safe(period=3, upperband=70, lowerband=30)

    def next(self):
        if super().next():
            return

        rsi0 = self.rsi[0]
        rsi1 = self.rsi[-1]
        rsi2 = self.rsi[-2]
        rsi3 = self.rsi[-3]

        if not self.position:
            rsi_30 = rsi0 > 30
            rsi_greater = rsi0 > 70
            rsi_great = 90 > rsi0 > 70
            good_story = rsi1 > 70 and rsi2 > 30 and rsi3 > 30
            #if rsi_great: # AAPL
            #if rsi_great and (rsi1 > 50 and rsi2 > 30): # POSSIBLE APPLE: sudden jump
            #if rsi_greater:
            #if rsi_greater and good_story:
            #if rsi_great and good_story: # NETFLIX
            if Data.indicator_list(self.rsi, [70, 30, 30]):
            #if self.positive_rsi_list([70, 30, 30]):
            #if (self.dataclose[0] > self.pp[-3]) and (self.dataclose[0] > self.pp[-2]):
            #if (self.sma < self.dataclose[0] and rsi_great):
                # self.log('BUY CREATE, %.2f' % self.dataclose[0])
                # self.log('RSI: %.2f, %.2f, %.2f, %.2f' % (rsi0, rsi1, rsi2, rsi3))

                self.order = self.buy()

        else:
            rsi_less = rsi0 < 30
            take_percents = self.profit_percents > 1
            #if take_percents:
            #if take_percents or rsi_less:
            #if rsi_less:
            #if two_bars or rsi_less:
            if self.two_bars and rsi_less and take_percents:  # NETFLIX
            #if (two_bars and rsi_less and take_percents) or (rsi0 < 30 and rsi1 < 30):  # NETFLIX
            #if (self.sma > self.dataclose[0] and self.sma > self.dataclose[-1]) or take_percents:
                # self.log('SELL CREATE, %.2f' % self.dataclose[0])

                self.order = self.sell()
