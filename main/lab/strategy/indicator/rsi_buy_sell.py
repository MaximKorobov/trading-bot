#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base
from main.lab.tools.data import Data


class RSIBuySell(Base):

    def strategy_urls(self):
        return []

    def strategy_info(self):
        return "Human-made strategy based on history observations"

    def __init__(self):
        super().__init__()

        self.sma = bt.ind.SMA(self.datas[0], period=15)
        self.rsi = bt.ind.RSI_Safe(period=3, upperband=70, lowerband=30)

    def next(self):
        if super().next():
            return

        if not self.position:
            if Data.indicator_list(self.rsi, [70, 50, 30, 30, 30]):
                self.order = self.buy()
            elif Data.indicator_list(self.rsi, [-30, -30, -70, -70, -70]):
                self.order = self.sell()

        else:
            if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                if self.position.size > 0:
                    self.order = self.sell()
                else:
                    self.order = self.buy()
