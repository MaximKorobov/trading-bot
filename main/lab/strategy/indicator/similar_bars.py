#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from main.lab.strategy.base import Base


class SimilarBars(Base):

    params = (
        ('bars', 3),
    )

    def strategy_urls(self):
        return [""]

    def strategy_info(self):
        return "Based on history observations of E.On German share. BAD SHARPE RATIO AND RESULTS"

    def __init__(self):
        super().__init__()
        self.bars = int(self.params.bars)

    def should_make_action(self, should_buy):
        sb = True

        for index in range(0, self.params.bars):
            close_price = self.datas[0].close[-index]
            open_price = self.datas[0].open[-index]
            if (open_price > close_price) == should_buy:
                sb = False
                break

        return sb

    def next(self):
        if super().next():
            return

        if not self.position:
            if self.should_make_action(should_buy=True):
                self.order = self.buy()
            elif self.should_make_action(should_buy=False):
                self.order = self.sell()

            # if self.should_make_action(should_buy=True):
            #     self.order = self.buy()

            # sb = True
            # for index in range(0, self.params.bars):
            #     close_price = self.datas[0].close[-index]
            #     open_price = self.datas[0].open[-index]
            #     if open_price > close_price:
            #         sb = False
            #         break
            # if sb:
            #     self.order = self.buy()

            # ss = True
            # for index in range(0, self.params.bars):
            #     close_price = self.datas[0].close[-index]
            #     open_price = self.datas[0].open[-index]
            #     if (open_price > close_price) == False:
            #         ss = False
            #         break
            #
            # if ss:
            #     self.order = self.sell()

            # if self.should_make_action(should_buy=False):
            #     self.order = self.sell()

        else:
            take_percents = self.profit_percents > 3
            loose_percents = self.profit_percents < -1

            if take_percents or loose_percents:
                self.order = self.close()

            # if self.position.size > 0:
            #     if self.should_make_action(should_buy=False):
            #         self.close()
            # else:
            #     if self.should_make_action(should_buy=True):
            #         self.close()
