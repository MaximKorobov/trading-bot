#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base
from main.lab.tools.data import Data


class BBands(Base):

    params = (
        ('rsi', 5),
        ('mode', 'default'),
    )

    def strategy_urls(self):
        return ["http://strategy4you.ru/strategii-na-osnove-polos-bollindzhera/torgovla-polosi-bollindzhera.html"]

    def strategy_info(self):
        pass

    def __init__(self):
        super().__init__()

        self.b_bands = bt.ind.BollingerBands()
        self.rsi = bt.ind.RSI_Safe(period=self.params.rsi, upperband=70, lowerband=30)

        self.mode = self.params.mode

    def check_open_position(self, bot):
        if self.mode == "default":
            return self.data_close[-1] < bot < self.data_close[0]
        elif self.mode == "rsi_support":
            return self.data_close[-1] < bot and self.data_close[-1] < self.data_close[0]

    def check_close_position(self, bot, top):
        if self.mode == "default":
            return self.data_close[-1] > top > self.data_close[0]
        elif self.mode == "rsi_support":
            return self.two_bars and self.data_close[-1] < bot and Data.i_l(self.rsi, [-30, -30])

    def next(self):
        if super().next():
            return

        if self.b_bands:
            indicator = self.getindicators()[0]
            # bot = self.b_bands[0]
            bot = indicator.lines.bot[0]
            top = indicator.lines.top[0]

            if not self.position:
                # if self.data_close[-1] < bot and Data.i_l(self.rsi, [-30, -30]):
                if self.check_open_position(bot):
                    self.order = self.buy()
                # elif self.data_close[0] > top:
                #     self.order = self.sell()
            else:
                # if two_bars and (5 < profit_percents or profit_percents < -100):
                # if (two_bars and self.data_close[0] < bot) or (5 < profit_percents):
                if self.check_close_position(bot, top):
                # if self.two_bars and self.data_close[0] > top:
                # if self.two_bars and self.data_close[-1] < bot and self.data_close[-1] < self.data_close[0]:
                    if self.position.size > 0:
                        self.order = self.sell()
                    else:
                        self.order = self.buy()
