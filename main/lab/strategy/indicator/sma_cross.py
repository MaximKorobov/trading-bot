#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base


class SMACross(Base):

    params = (
        ('sma1', 2),
        ('sma2', 10),
    )

    def strategy_urls(self):
        pass

    def strategy_info(self):
        return "This is a long-only strategy which operates on a moving average cross"

    def __init__(self):
        super().__init__()

        sma_fast = bt.ind.SMA(self.datas[0], period=self.params.sma1)
        sma_slow = bt.ind.SMA(self.datas[0], period=self.params.sma2)
        self.signal = bt.ind.CrossOver(sma_fast, sma_slow)

        self.rsi = bt.ind.RSI_Safe(period=3, upperband=70, lowerband=30)

    def next(self):
        if super().next():
            return

        # if self.signal > 0.0:
        #     if self.position:
        #         self.log('CLOSE SHORT , %.2f' % self.data.close[0])
        #         self.close()
        #
        #     self.log('BUY CREATE , %.2f' % self.data.close[0])
        #     self.buy()
        #
        # elif self.signal < 0.0:
        #     if self.position:
        #         self.log('CLOSE LONG , %.2f' % self.data.close[0])
        #         self.close()
        #
        #     self.log('SELL CREATE , %.2f' % self.data.close[0])
        #     self.sell()

        if not self.position:
            if self.signal > 0:
                self.order = self.buy()
                # elif self.signal < 0:
                #    self.order = self.sell()
        else:
            same_sign = self.position.size * self.signal < 0
            if same_sign:
                # if (two_bars and (3 < profit_percents or profit_percents < -1)) or same_sign:
                # if two_bars:
                if self.position.size > 0:
                    self.order = self.sell()
                else:
                    self.order = self.buy()
