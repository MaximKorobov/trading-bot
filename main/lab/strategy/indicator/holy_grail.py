#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base
from main.lab.tools.data import Data


class HolyGrail(Base):

    params = (
        ('ema', 20),
        ('adx', 14),
    )

    def strategy_urls(self):
        return ["http://forex-invest.tv/strategii-nachinayushim/strategiya-foreks-nachinayushchim-svyatoj-graal.html"]

    def strategy_info(self):
        return "ADX cross the level and close price higher EMA"

    def __init__(self):
        super().__init__()

        self.ema = bt.ind.ExponentialMovingAverage(self.datas[0], period=self.params.ema)
        self.adx = bt.ind.PlusDirectionalIndicator(period=self.params.adx)

    def next(self):
        if super().next():
            return

        if not self.position:
            if Data.indicator_list(self.adx, [20, 20, -20]) and self.ema < self.data_close[0]:
                self.order = self.buy()
            #elif self.adx[-2] < 20 < self.adx[-1] and self.ema < self.data_close[0]:
            #    self.order = self.sell()
        else:
            if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
            # if two_bars:
            #if self.ema > self.data_close[0]:
                if self.position.size > 0:
                    self.order = self.sell()
                else:
                    self.order = self.buy()
