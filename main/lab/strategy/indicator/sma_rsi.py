#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base


class SMA_RSI(Base):

    params = (
        ('sma', 5),
        ('rsi', 5),
    )

    def strategy_urls(self):
        return [
            "http://forex-invest.tv/strategii-foreks-po-metodu-skolzyashix-srednix/strategiya-po-metodu"
            "-skolzyashchikh-srednikh-sma-rsi.html",
            "http://finzz.ru/koefficient-sharpa-formula-rascheta-primer.html"
        ]

    def strategy_info(self):
        pass

    def __init__(self):
        super().__init__()

        self.sma = bt.ind.SMA(self.datas[0], period=self.params.sma)
        self.rsi = bt.ind.RSI_Safe(period=self.params.rsi, upperband=70, lowerband=30)

    def next(self):
        if super().next():
            return

        if not self.position:
            if self.sma[-1] > self.data_close[0] > self.sma[0] and self.rsi[0] > 50:
                self.order = self.buy()
            elif self.sma[-1] < self.data_close[0] < self.sma[0] and self.rsi[0] < 50:
                self.order = self.sell()
        else:
            if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
            #if two_bars:
                if self.position.size > 0:
                    self.order = self.sell()
                else:
                    self.order = self.buy()
