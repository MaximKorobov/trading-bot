#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import backtrader as bt

from main.lab.strategy.base import Base


class MACD_ADX(Base):

    params = (
        ('period_me1', 3),
        ('period_me2', 9),
        ('period_signal', 16),
        ('adx', 18),
    )

    def strategy_urls(self):
        return ["http://fox-trader.ru/startegiya-macd-adx.html"]

    def strategy_info(self):
        pass

    def __init__(self):
        super().__init__()

        self.macd = bt.ind.MACD(self.datas[0],
                               period_me1=self.params.period_me1,
                               period_me2=self.params.period_me2,
                               period_signal=self.params.period_signal)
        self.adx_plus = bt.ind.PlusDirectionalIndicator(period=self.params.adx)
        self.adx_minus = bt.ind.MinusDirectionalIndicator(period=self.params.adx)

    def next(self):
        if super().next():
            return

        if not self.position:
            if self.macd[-1] < 0 < self.macd[0] and self.adx_plus[0] > self.adx_minus[0]:
                self.order = self.buy()
            elif self.macd[-1] > 0 > self.macd[0] and self.adx_plus[0] < self.adx_minus[0]:
                self.order = self.sell()
        else:
            if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
            #if two_bars:
                if self.position.size > 0:
                    self.order = self.sell()
                else:
                    self.order = self.buy()
