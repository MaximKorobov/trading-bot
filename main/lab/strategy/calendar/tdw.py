#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from backtrader.utils import num2date

from main.lab.strategy.base import Base


class TDW(Base):

    params = (
        ('buy_day', 2),
        ('sell_day', 3),
    )

    def strategy_urls(self):
        return []

    def strategy_info(self):
        return ""

    def next(self):
        if super().next():
            return

        today = num2date(self.datas[0].datetime[0])
        today_weekday = today.weekday()

        if not self.position:
            # if today < datetime.datetime(2017, 1, 1):
            #     return

            if today_weekday == self.params.buy_day:
                self.buy()
            elif today_weekday == self.params.sell_day:
                self.sell()
        else:
            if self.two_bars and (3 < self.profit_percents or self.profit_percents < -1):
                self.order = self.close()
            # TODO: improve: IF self.position.size > 0 THEN SELL
            # if self.position.size > 0:
            #     if today_weekday == self.params.buy_day:
            #         self.close()
            # else:
            #     if today_weekday == self.params.sell_day:
            #         self.close()
            #
