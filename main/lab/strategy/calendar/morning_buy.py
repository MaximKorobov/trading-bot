#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from backtrader.utils import num2date

from main.lab.strategy.base import Base


class MorningBuy(Base):

    params = (
        ('hour_offset', 0),
        ('open_hour', 8),
        ('close_hour', 2),
    )

    def strategy_urls(self):
        return []

    def strategy_info(self):
        return "Maxim Zhdanov (max_cmc@skype) original idea"

    def __init__(self):
        super().__init__()
        self.hour_offset = self.params.hour_offset

        self.open_hour = self.params.open_hour
        self.close_hour = self.params.close_hour

        if self.close_hour <= self.open_hour:
            super().stop()

    def next(self):
        if super().next():
            return

        # Today data
        price = self.datas[0].close[0]
        date_time = num2date(self.datas[0].datetime[0])
        time = date_time.time()
        hour = time.hour + self.hour_offset

        # Previous data
        prev_date_last_index = 0
        prev_date = 0
        for i in range(1, min(23, len(self.datas[0]))):
            prev_date = num2date(self.datas[0].datetime[-i])
            diff = date_time.day - prev_date.day
            if diff >= 1:
                prev_date_last_index = -i
                break

        if prev_date_last_index == 0:
            return
        prev_date_close_price = self.datas[0].close[prev_date_last_index]

        # Strategy
        if not self.position:
            if self.open_hour + 1 > hour >= self.open_hour:
                if price > prev_date_close_price:
                    self.order = self.buy()
                else:
                    pass
                    print("Skip buy action: %.2f (%s) -> %.2f (%s)" %
                          (prev_date_close_price, prev_date,
                           price, date_time))

        else:
            if self.close_hour + 1 > hour >= self.close_hour:
                self.order = self.sell()
