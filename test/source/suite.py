#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest as ut

from test.source.index import TestIndexLoader
from test.source.inventory import TestInventories

from test.source.earning import TestEarningLoader, TestEarningDates


class SourceTests:

    @staticmethod
    def get_tests():
        return [
            TestEarningLoader(), TestEarningDates(),
            TestInventories(),
            TestIndexLoader()
        ]

if __name__ == "__main__":
    runner = ut.TextTestRunner()
    test_suite = ut.TestSuite()

    tests = SourceTests.get_tests()
    for test in tests:
        test_suite.addTest(test)

    runner.run(test_suite)
