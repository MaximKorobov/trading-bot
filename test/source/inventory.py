#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest as ut

import datetime

from main.source.investing.inventory import Loader
from main.source.investing.inventory.source import API, Crude, Cushing, Distillates, Gasoline, Imports, RigsCount
from main.tool.date import Date


class TestInventories(ut.TestCase):

    def __init__(self):
        super().__init__()

        self.offline_mode = 0

    def runTest(self):
        loader = Loader()
        events_count = 6

        print("Inventories:")
        source = Crude()
        print("\t" + source.__class__.__name__)
        data = loader.get_data(source, self.offline_mode, datetime.datetime.now())
        self.assertTrue(len(data) == events_count)

        sources = [API(), Crude(), Gasoline(), Cushing(), Distillates(), Imports(), RigsCount()]
        # sources = [Crude()]
        for source in sources:
            print("\t" + source.__class__.__name__)
            from_date, to_date = Date.last_months(12)
            data = loader.get_data(source, self.offline_mode, from_date)
            self.assertTrue(len(data) % events_count == 0)
            count = len(data) / events_count
            self.assertTrue(count - int(count) == 0)
