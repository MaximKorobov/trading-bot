#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest as ut
from datetime import datetime

from main.source.investing.earning import Loader
from main.source.investing.earning.earning_dates import EarningDates
from main.source.investing.earning.source import Netflix, Apple, Google
from main.tool.date import Date


class TestEarningLoader(ut.TestCase):

    def __init__(self):
        super().__init__()

        self.offline_mode = 0

    def runTest(self):
        print("Earning load")

        data_sources = [Netflix(), Apple(), Google()]

        earning_loader = Loader()
        for data_source in data_sources:
            print("\t" + data_source.name)

            print("\t\tLast 2 years", end=" - ")
            from_date, to_date = Date.last_years(3)
            data = earning_loader.get_data(data_source, self.offline_mode, from_date)
            self.assertTrue(data is not None)
            self.assertTrue(len(data) != 0)
            print("%d elements" % len(data))

            print("\t\tLast elements", end=" - ")
            data = earning_loader.get_data(data_source, self.offline_mode, datetime.now())
            self.assertTrue(data is not None)
            self.assertTrue(len(data) != 0)
            self.assertTrue(len(data) == 6)
            print("%d elements" % len(data))


class TestEarningDates(ut.TestCase):

    def __init__(self):
        super().__init__()
        self._testMethodDoc = "Check that EarningDays generate right skip days count before and after earnings"

        self.offline_mode = 0

    def runTest(self):
        print("Earning days")

        data_source = Netflix()
        ed1 = EarningDates(self.offline_mode, data_source, 0, 1, 0)
        skip_days = ed1.get_skip_dates()

        ed2 = EarningDates(self.offline_mode, data_source, 0, 2, 3)
        skip_days_more = ed2.get_skip_dates()

        diff_days = len(skip_days_more) - len(skip_days)
        self.assertFalse(diff_days == 4)
        self.assertFalse(len(skip_days) == 5)
