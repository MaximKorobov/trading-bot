#
# Copyright 2016-2017, Maxim Korobov
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import unittest as ut
from datetime import datetime

from main.source.investing.index import Loader
from main.source.investing.index.source.dax import Dax
from main.source.investing.index.source.nasdaq import Nasdaq
from main.source.investing.index.source.sp_500 import SP500


class TestIndexLoader(ut.TestCase):

    def __init__(self):
        super().__init__()

        self.offline_mode = 0

    def runTest(self):
        print("Index components")

        data_sources = [Dax(), SP500(), Nasdaq()]
        for data_source in data_sources:
            index_loader = Loader()
            data = index_loader.get_data(data_source, self.offline_mode, datetime.now())
            self.assertTrue(data is not None)
            self.assertTrue(len(data) != 0)

            # for item in data:
            #     print(item)
